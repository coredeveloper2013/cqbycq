-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 13, 2018 at 03:56 PM
-- Server version: 5.7.21
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fameaccessories`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_ship_methods`
--

DROP TABLE IF EXISTS `admin_ship_methods`;
CREATE TABLE IF NOT EXISTS `admin_ship_methods` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `courier_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_ship_methods`
--

INSERT INTO `admin_ship_methods` (`id`, `name`, `courier_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'UPS Ground', 1, '2018-06-01 03:13:50', '2018-06-01 03:13:50', NULL),
(2, 'UPS 23', 1, '2018-06-01 03:15:23', '2018-06-01 03:20:10', NULL),
(3, 'UPS 3', 1, '2018-06-01 03:15:34', '2018-06-01 03:15:34', NULL),
(4, 'erw', 1, '2018-06-01 03:20:58', '2018-06-01 03:21:00', '2018-06-01 03:21:00'),
(5, 'Truck', 3, '2018-06-01 03:38:12', '2018-07-02 09:19:53', '2018-07-02 09:19:53'),
(6, 'asdf3', 4, '2018-07-02 09:28:36', '2018-07-02 09:33:19', '2018-07-02 09:33:19');

-- --------------------------------------------------------

--
-- Table structure for table `block_users`
--

DROP TABLE IF EXISTS `block_users`;
CREATE TABLE IF NOT EXISTS `block_users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `vendor_meta_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `block_users`
--

INSERT INTO `block_users` (`id`, `user_id`, `vendor_meta_id`, `created_at`, `updated_at`) VALUES
(3, 14, 1, '2018-06-26 05:31:46', '2018-06-26 05:31:46');

-- --------------------------------------------------------

--
-- Table structure for table `body_sizes`
--

DROP TABLE IF EXISTS `body_sizes`;
CREATE TABLE IF NOT EXISTS `body_sizes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `body_sizes`
--

INSERT INTO `body_sizes` (`id`, `name`, `parent_category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Junior', 1, '2018-05-17 04:13:39', '2018-05-22 14:18:18', NULL),
(2, 'sdf', 1, '2018-05-17 05:00:37', '2018-05-17 05:00:40', '2018-05-17 05:00:40'),
(3, 'Young Contemporary', 1, '2018-05-17 06:39:49', '2018-05-22 14:18:31', NULL),
(4, '222', 0, '2018-05-17 06:42:29', '2018-05-17 06:42:35', '2018-05-17 06:42:35'),
(5, 'Missy', 1, '2018-05-22 14:18:43', '2018-05-22 14:18:43', NULL),
(6, 'Plus Size', 1, '2018-05-22 14:18:51', '2018-05-22 14:18:51', NULL),
(7, 'Maternity', 1, '2018-05-22 14:18:59', '2018-05-22 14:18:59', NULL),
(8, 'tt', 3, '2018-06-22 03:13:35', '2018-06-22 03:36:26', '2018-06-22 03:36:26');

-- --------------------------------------------------------

--
-- Table structure for table `buyer_shipping_addresses`
--

DROP TABLE IF EXISTS `buyer_shipping_addresses`;
CREATE TABLE IF NOT EXISTS `buyer_shipping_addresses` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `store_no` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `state_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_id` int(11) NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `commercial` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `buyer_shipping_addresses`
--

INSERT INTO `buyer_shipping_addresses` (`id`, `user_id`, `default`, `store_no`, `location`, `address`, `unit`, `city`, `state_id`, `state_text`, `zip`, `country_id`, `phone`, `fax`, `commercial`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 14, 1, 'fd', 'US', 'Raj', NULL, 'gsdfgfga', 10, NULL, '5656', 1, '4576767', '4545', 0, '2018-06-14 18:00:00', '2018-06-22 10:58:40', NULL),
(2, 19, 1, 'erwer', 'US', '45345', NULL, '45', 4, NULL, '34535', 1, '4545', NULL, 0, '2018-06-22 07:18:27', '2018-06-22 07:18:27', NULL),
(7, 14, 0, 'sadfa', 'US', 'asdfa', NULL, 'asdf', 4, NULL, 'asdf', 1, 'rfer', NULL, 0, '2018-06-22 09:49:57', '2018-06-22 10:58:40', NULL),
(6, 14, 0, 'bad 2', 'US', 'asdf', '2', 'dfadf', 1, NULL, '3er', 1, '3434', NULL, 1, '2018-06-22 09:49:27', '2018-06-22 10:58:40', NULL),
(8, 14, 0, 'sadfa', 'US', 'asdfa', NULL, 'asdf', 4, NULL, 'asdf', 1, 'rfer', NULL, 0, '2018-06-22 09:49:58', '2018-06-22 10:58:40', NULL),
(9, 14, 0, 'asdf', 'US', 'asdf', NULL, 'asdf', 5, NULL, 'sdf', 1, '34', NULL, 0, '2018-06-22 09:50:44', '2018-06-22 10:11:47', '2018-06-22 10:11:47'),
(10, 14, 0, NULL, 'US', 'fdaf', 'd', 'dfads', 4, NULL, 'asdfad', 1, 'asdf', NULL, 0, '2018-06-22 10:58:27', '2018-06-22 10:58:40', NULL),
(11, 14, 0, 'BD', 'INT', 'sdfdfdf', '45', 'rtrtr', NULL, 'erer', 'ter', 20, 'erer', NULL, 0, '2018-06-22 11:31:30', '2018-06-22 11:31:30', NULL),
(13, 14, 0, 'dd', 'US', 'jhaka naka', NULL, 'rer', 3, NULL, 'rer', 1, '3434', NULL, 0, '2018-06-23 10:15:59', '2018-06-23 10:15:59', NULL),
(14, 21, 1, 'erwer', 'US', 'qwer', NULL, 'qwer', 4, NULL, 'qwer', 1, 'qwer', 'qwer', 0, '2018-07-12 07:30:13', '2018-07-12 07:30:13', NULL),
(15, 22, 1, 'adsf', 'US', 'asdf', NULL, 'asdf', 3, NULL, 'asdf', 1, 'aasdf', 'asdf', 0, '2018-07-12 07:57:38', '2018-07-12 07:57:38', NULL),
(16, 14, 0, NULL, 'US', 'New tt', NULL, 'asdf', 4, NULL, 'asdf', 1, 'fasd', 'adsf', 0, '2018-07-13 09:11:48', '2018-07-13 09:11:48', NULL),
(17, 14, 0, NULL, 'US', 'tttete', NULL, 'wer', 4, NULL, 'wer', 1, '234', '234', 0, '2018-07-13 09:16:53', '2018-07-13 09:16:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cart_items`
--

DROP TABLE IF EXISTS `cart_items`;
CREATE TABLE IF NOT EXISTS `cart_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=78 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `parent`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Jewelry', 0, 1, '2018-05-15 03:22:30', '2018-07-09 07:39:55', NULL),
(2, 'SHOES', 9, 1, '2018-05-15 03:22:39', '2018-05-22 13:35:29', '2018-05-22 13:35:29'),
(3, 'ACCESSORIES', 0, 3, '2018-05-15 03:22:42', '2018-06-30 12:52:55', '2018-06-30 12:52:55'),
(4, 'Body Jewelry', 43, 2, '2018-05-15 03:22:44', '2018-06-29 09:32:59', NULL),
(5, 'HANDBAGS', 0, 4, '2018-05-15 03:22:47', '2018-06-28 04:12:25', '2018-06-28 04:12:25'),
(6, 'Bracelets', 43, 3, '2018-05-15 03:22:50', '2018-06-29 09:32:59', NULL),
(7, 'Bagpack', 44, 1, '2018-05-15 03:22:52', '2018-06-07 13:55:05', NULL),
(8, 'Belts', 43, 1, '2018-05-15 03:22:54', '2018-06-29 09:32:59', NULL),
(9, 'SHOES', 0, 2, '2018-05-15 03:22:57', '2018-06-30 12:52:46', '2018-06-30 12:52:46'),
(10, 'Necklaces', 1, 1, '2018-05-15 03:59:50', '2018-07-09 07:39:55', NULL),
(11, 'Bucket', 44, 2, '2018-05-15 04:00:29', '2018-06-07 13:55:05', NULL),
(12, 'Long Length', 10, 1, '2018-05-22 02:39:30', '2018-07-09 07:39:55', NULL),
(13, 'Made in Korea', 10, 2, '2018-05-22 02:39:47', '2018-07-09 07:39:55', NULL),
(14, 'Earrings', 1, 2, '2018-05-22 02:40:09', '2018-07-09 07:39:55', NULL),
(15, 'Dangle/Pendant', 14, 1, '2018-05-22 02:40:18', '2018-07-09 07:39:55', NULL),
(16, 'Adjustable Clip On', 14, 2, '2018-05-22 02:40:26', '2018-07-09 07:39:55', NULL),
(17, 'Mid Length', 10, 3, '2018-05-22 13:17:29', '2018-07-09 07:39:55', NULL),
(18, 'Chokers', 10, 4, '2018-05-22 13:17:42', '2018-07-09 07:39:55', NULL),
(19, 'Statement Necklaces', 10, 5, '2018-05-22 13:17:58', '2018-07-09 07:39:55', NULL),
(20, 'Seamless', 10, 6, '2018-05-22 13:18:10', '2018-07-09 07:36:15', '2018-07-09 07:36:15'),
(21, 'Shirt & Blouse', 10, 7, '2018-05-22 13:18:25', '2018-07-09 07:36:19', '2018-07-09 07:36:19'),
(22, 'Tank & Tube Tops', 10, 8, '2018-05-22 13:19:07', '2018-07-09 07:36:22', '2018-07-09 07:36:22'),
(23, 'T-Shirt & Polo', 10, 9, '2018-05-22 13:19:28', '2018-07-09 07:36:25', '2018-07-09 07:36:25'),
(24, 'Tunics', 10, 10, '2018-05-22 13:19:40', '2018-07-09 07:36:27', '2018-07-09 07:36:27'),
(25, 'Stud', 14, 3, '2018-05-22 13:20:17', '2018-07-09 07:39:55', NULL),
(26, 'Hoop', 14, 4, '2018-05-22 13:20:30', '2018-07-09 07:39:55', NULL),
(27, 'Earcuffs', 14, 5, '2018-05-22 13:20:42', '2018-07-09 07:39:55', NULL),
(28, 'Earring Sets', 14, 6, '2018-05-22 13:21:00', '2018-07-09 07:39:55', NULL),
(29, 'JACKETS / OUTWEAR', 1, 3, '2018-05-22 13:30:21', '2018-07-09 07:39:35', '2018-07-09 07:39:35'),
(30, 'Cape & Poncho', 29, 1, '2018-05-22 13:30:47', '2018-07-09 07:39:02', NULL),
(31, 'Coats', 29, 2, '2018-05-22 13:31:00', '2018-07-09 07:39:02', NULL),
(32, 'Hoodies', 29, 3, '2018-05-22 13:31:10', '2018-07-09 07:39:02', NULL),
(33, 'Jackets & Blazer', 29, 4, '2018-05-22 13:31:29', '2018-07-09 07:39:02', NULL),
(34, 'Shrugs & Cardigans', 29, 5, '2018-05-22 13:31:46', '2018-07-09 07:39:02', NULL),
(35, 'Sweaters', 29, 6, '2018-05-22 13:32:00', '2018-07-09 07:39:02', NULL),
(36, 'Vests', 29, 7, '2018-05-22 13:32:17', '2018-06-30 12:52:49', '2018-06-30 12:52:49'),
(37, 'Booties', 42, 1, '2018-05-22 13:33:58', '2018-06-29 09:32:59', NULL),
(38, 'Boots', 42, 2, '2018-05-22 13:34:11', '2018-06-29 09:32:59', NULL),
(39, 'Dress Shoes', 42, 3, '2018-05-22 13:34:25', '2018-06-29 09:32:59', NULL),
(40, 'Flats', 42, 4, '2018-05-22 13:34:40', '2018-06-29 09:32:59', NULL),
(41, 'Crossbody Bags', 44, 3, '2018-05-22 13:39:00', '2018-06-07 13:55:05', NULL),
(42, 'SHOES', 9, 1, '2018-05-22 13:45:19', '2018-06-29 09:32:59', NULL),
(43, 'ACCESSORIES', 3, 1, '2018-05-22 13:45:58', '2018-06-29 09:32:59', NULL),
(44, 'HANDBAGS', 5, 1, '2018-05-22 13:46:15', '2018-06-07 13:55:05', NULL),
(45, 'MEN', 0, 4, '2018-05-22 13:46:33', '2018-06-30 12:52:43', '2018-06-30 12:52:43'),
(46, 'MEN', 45, 1, '2018-05-22 13:46:53', '2018-06-29 09:32:59', NULL),
(47, 'Casual Shirts', 46, 1, '2018-05-22 13:47:15', '2018-06-29 09:32:59', NULL),
(48, 'Denim', 46, 2, '2018-05-22 13:47:36', '2018-06-29 09:32:59', NULL),
(49, 'Dress Shirts', 46, 3, '2018-05-22 13:47:50', '2018-06-29 09:32:59', NULL),
(50, 'Graphic', 46, 4, '2018-05-22 13:48:04', '2018-06-29 09:32:59', NULL),
(51, 'KIDS', 0, 5, '2018-05-22 13:48:21', '2018-06-30 12:52:23', '2018-06-30 12:52:23'),
(52, 'KIDS', 51, 1, '2018-05-22 13:48:26', '2018-06-29 09:32:59', NULL),
(53, 'Boys', 52, 1, '2018-05-22 13:48:56', '2018-06-29 09:32:59', NULL),
(54, 'Girls', 52, 2, '2018-05-22 13:49:03', '2018-06-29 09:32:59', NULL),
(55, 'Infants', 52, 3, '2018-05-22 13:49:21', '2018-06-29 09:32:59', NULL),
(56, 'MORE', 0, 7, '2018-05-22 13:50:00', '2018-06-30 12:52:14', '2018-06-30 12:52:14'),
(57, 'MORE', 56, 1, '2018-05-22 13:50:04', '2018-06-29 09:32:59', NULL),
(58, 'Cosmetics', 57, 1, '2018-05-22 13:50:22', '2018-06-29 09:32:59', NULL),
(59, 'Fixtures/Display', 57, 2, '2018-05-22 13:50:33', '2018-06-29 09:32:59', NULL),
(60, 'Key Chains', 57, 3, '2018-05-22 13:50:43', '2018-06-29 09:32:59', NULL),
(61, 'test2', 0, 8, '2018-06-29 09:32:30', '2018-06-29 09:33:09', '2018-06-29 09:33:09'),
(62, 'Accessories', 0, 2, '2018-06-30 12:52:36', '2018-07-09 07:39:55', NULL),
(63, 'Second Level', 0, 3, '2018-06-30 12:53:11', '2018-07-09 07:42:20', '2018-07-09 07:42:20'),
(64, 'Second Level Item', 63, 1, '2018-06-30 12:53:22', '2018-07-09 07:39:55', NULL),
(65, 'Made in Korea', 14, 7, '2018-07-09 07:39:28', '2018-07-09 07:39:55', NULL),
(66, 'Beach Towels', 62, 1, '2018-07-09 07:41:16', '2018-07-09 07:41:16', NULL),
(67, 'Ponchos and Vests', 62, 2, '2018-07-09 07:41:29', '2018-07-09 07:41:29', NULL),
(68, 'Bags and Clutches', 62, 3, '2018-07-09 07:41:46', '2018-07-09 07:41:46', NULL),
(69, 'Handbags', 68, 1, '2018-07-09 07:42:35', '2018-07-09 07:42:35', NULL),
(70, 'Backpacks', 68, 2, '2018-07-09 07:42:44', '2018-07-09 07:42:44', NULL),
(71, 'Clutches', 68, 3, '2018-07-09 07:42:57', '2018-07-09 07:42:57', NULL),
(72, 'Cosmetic Bags', 68, 4, '2018-07-09 07:43:08', '2018-07-09 07:43:08', NULL),
(73, 'Coin Purse', 68, 5, '2018-07-09 07:43:17', '2018-07-09 07:43:17', NULL),
(74, 'Hand Made Handbags', 68, 6, '2018-07-09 07:43:35', '2018-07-09 07:43:35', NULL),
(75, 'Crossbody Bags', 68, 7, '2018-07-09 07:43:53', '2018-07-09 07:43:53', NULL),
(76, 'Fanny Packs', 68, 8, '2018-07-09 07:44:04', '2018-07-09 07:44:04', NULL),
(77, 'Parent', 0, 3, '2018-07-09 09:29:40', '2018-07-09 09:29:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category_banners`
--

DROP TABLE IF EXISTS `category_banners`;
CREATE TABLE IF NOT EXISTS `category_banners` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `vendor_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category_banners`
--

INSERT INTO `category_banners` (`id`, `type`, `vendor_id`, `category_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2018-06-07 15:19:59', '2018-06-07 15:19:59'),
(2, 2, 2, 1, '2018-06-07 15:25:45', '2018-06-07 15:25:45'),
(3, 3, 1, 1, '2018-06-07 15:25:49', '2018-06-07 15:25:49'),
(6, 3, 2, 1, '2018-06-07 15:30:23', '2018-06-07 15:30:23'),
(8, 2, 1, 1, '2018-06-08 10:28:20', '2018-06-08 10:28:20'),
(7, 1, 2, 1, '2018-06-08 09:09:28', '2018-06-08 09:09:28'),
(10, 1, 1, 0, '2018-06-11 05:49:31', '2018-06-11 05:49:31'),
(11, 1, 2, 0, '2018-06-11 12:52:37', '2018-06-11 12:52:37'),
(12, 2, 1, 0, '2018-06-11 12:52:40', '2018-06-11 12:52:40'),
(13, 2, 2, 0, '2018-06-11 12:52:44', '2018-06-11 12:52:44'),
(14, 3, 1, 0, '2018-06-11 12:52:48', '2018-06-11 12:52:48'),
(15, 3, 2, 0, '2018-06-11 12:52:51', '2018-06-11 12:52:51');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
CREATE TABLE IF NOT EXISTS `colors` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `master_color_id` int(11) NOT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbs_image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `name`, `status`, `master_color_id`, `image_path`, `thumbs_image_path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'BEIGE', 1, 14, NULL, NULL, '2018-05-11 09:11:15', '2018-06-25 07:55:30', NULL),
(5, 'BLACK MULTI', 1, 8, NULL, NULL, '2018-05-22 14:38:46', '2018-06-10 03:53:16', NULL),
(2, 'tet2', 1, 1, NULL, NULL, '2018-05-12 02:29:35', '2018-05-12 03:15:09', '2018-05-12 03:15:09'),
(3, 'DeepBlue2', 1, 4, NULL, NULL, '2018-05-17 10:36:52', '2018-05-17 10:36:59', '2018-05-17 10:36:59'),
(4, 'BLACK', 1, 8, NULL, NULL, '2018-05-18 10:55:03', '2018-06-10 03:34:39', NULL),
(6, 'BLUE', 1, 9, NULL, NULL, '2018-05-22 14:38:59', '2018-06-10 03:53:20', NULL),
(7, 'BLUE MULTI', 1, 9, NULL, NULL, '2018-05-22 14:39:12', '2018-06-10 03:53:25', NULL),
(8, 'BLUE/YELLOW', 1, 15, NULL, NULL, '2018-05-22 14:39:26', '2018-06-10 03:53:30', NULL),
(9, 'BLUSH', 1, 17, NULL, NULL, '2018-05-22 14:39:36', '2018-07-07 10:00:00', '2018-07-07 10:00:00'),
(10, 'BRICK', 1, 10, NULL, NULL, '2018-05-22 14:39:47', '2018-05-22 14:39:47', NULL),
(11, 'BROWN', 1, 10, NULL, NULL, '2018-05-22 14:40:01', '2018-06-10 03:53:39', NULL),
(12, 'CAMEL', 1, 15, NULL, NULL, '2018-05-22 14:40:09', '2018-06-10 03:53:43', NULL),
(13, 'CHARCOAL', 1, 8, NULL, NULL, '2018-05-22 14:40:18', '2018-05-22 14:41:13', NULL),
(14, 'CORAL', 1, 16, NULL, NULL, '2018-05-22 14:40:31', '2018-06-10 03:53:51', NULL),
(15, 'GREEN', 1, 13, NULL, NULL, '2018-05-23 04:14:51', '2018-06-10 03:53:56', NULL),
(16, 'RED', 1, 7, NULL, NULL, '2018-05-23 04:14:57', '2018-06-10 03:54:07', NULL),
(17, 'WHITE', 1, 21, NULL, NULL, '2018-05-23 04:15:04', '2018-05-23 04:15:04', NULL),
(18, 'PINK', 1, 17, NULL, NULL, '2018-05-23 04:24:10', '2018-06-10 03:54:02', NULL),
(19, 'JADE', 1, 14, NULL, NULL, '2018-05-23 15:23:45', '2018-05-23 15:23:45', NULL),
(20, 'WHITE', 1, 21, NULL, NULL, '2018-05-23 15:24:04', '2018-05-23 15:24:04', NULL),
(21, 'YELLOW', 1, 22, NULL, NULL, '2018-05-23 15:24:18', '2018-05-23 15:24:18', NULL),
(22, 'PINK', 1, 17, NULL, NULL, '2018-05-23 15:27:43', '2018-05-23 15:27:43', NULL),
(23, 'MINT', 1, 13, NULL, NULL, '2018-05-23 15:28:02', '2018-05-23 15:28:02', NULL),
(24, '122', 1, 9, NULL, NULL, '2018-05-26 09:47:53', '2018-05-26 09:53:27', '2018-05-26 09:53:27'),
(25, '3', 1, 10, NULL, NULL, '2018-05-26 09:52:58', '2018-05-26 09:53:24', '2018-05-26 09:53:24'),
(26, 'test', 1, 9, NULL, NULL, '2018-06-10 03:15:20', '2018-06-22 05:36:56', '2018-06-22 05:36:56'),
(27, 'tttt', 1, 8, '/images/vendors/1/colors/19af7a90-6f29-11e8-8d9e-99bd1237045b.jpg', '/images/vendors/1/colors/thumbs/19af7a90-6f29-11e8-8d9e-99bd1237045b.jpg', '2018-06-13 10:40:13', '2018-06-13 15:46:47', '2018-06-13 15:46:47'),
(28, 'fdf', 1, 8, NULL, NULL, '2018-06-22 05:37:02', '2018-06-22 05:37:10', '2018-06-22 05:37:10'),
(29, 'sdf', 1, 9, '/images/vendors/1/colors/9fb374f0-7610-11e8-9d3a-cf28bc1263a7.jpg', '/images/vendors/1/colors/thumbs/9fb374f0-7610-11e8-9d3a-cf28bc1263a7.jpg', '2018-06-22 05:37:20', '2018-06-22 05:37:24', '2018-06-22 05:37:24'),
(30, 'rt', 1, 8, NULL, NULL, '2018-06-25 07:56:25', '2018-06-25 07:56:28', '2018-06-25 07:56:28'),
(31, 'trt', 1, 8, NULL, NULL, '2018-06-25 10:52:40', '2018-06-25 10:52:43', '2018-06-25 10:52:43'),
(35, 'sadf', 1, 9, NULL, NULL, '2018-06-28 09:40:47', '2018-06-28 09:40:47', NULL),
(36, 'asdf', 1, 20, NULL, NULL, '2018-06-28 09:43:58', '2018-06-28 09:43:58', NULL),
(37, 'bsdf', 1, 19, NULL, NULL, '2018-06-28 09:55:47', '2018-06-28 09:55:47', NULL),
(38, 'v', 1, 20, NULL, NULL, '2018-06-28 10:42:02', '2018-06-28 10:42:02', NULL),
(39, 'fasd', 1, 8, NULL, NULL, '2018-06-28 12:21:52', '2018-06-28 12:21:52', NULL),
(40, 'bl', 1, 13, NULL, NULL, '2018-06-28 13:26:09', '2018-06-28 13:26:09', NULL),
(41, 'gfsad22', 1, 8, '/images/colors/38155b90-7bb6-11e8-b7ad-a176e0cfc740.jpg', '/images/colors/thumbs/38155b90-7bb6-11e8-b7ad-a176e0cfc740.jpg', '2018-06-29 10:05:08', '2018-06-29 10:05:44', '2018-06-29 10:05:44'),
(42, 'asdfdf', 1, 20, NULL, NULL, '2018-06-30 10:28:36', '2018-06-30 10:28:36', NULL),
(43, 'GOLD', 1, 11, NULL, NULL, '2018-07-09 07:50:58', '2018-07-09 07:50:58', NULL),
(44, 'SILVER', 1, 19, NULL, NULL, '2018-07-09 07:51:04', '2018-07-09 07:51:04', NULL),
(45, 'AMBER', 1, 8, NULL, NULL, '2018-07-09 07:53:03', '2018-07-09 07:53:03', NULL),
(46, 'GREY', 1, 12, NULL, NULL, '2018-07-09 07:53:14', '2018-07-09 07:53:14', NULL),
(47, 'GUNMETAL', 1, 19, NULL, NULL, '2018-07-09 07:53:22', '2018-07-09 07:53:22', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `color_item`
--

DROP TABLE IF EXISTS `color_item`;
CREATE TABLE IF NOT EXISTS `color_item` (
  `item_id` int(11) NOT NULL,
  `color_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `color_item`
--

INSERT INTO `color_item` (`item_id`, `color_id`) VALUES
(2, 4),
(3, 4),
(3, 40),
(4, 4),
(5, 43),
(5, 44),
(6, 45),
(6, 46),
(6, 47),
(7, 46),
(7, 44),
(8, 4),
(9, 46),
(9, 44);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=243 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`) VALUES
(1, 'US', 'United States'),
(2, 'CA', 'Canada'),
(3, 'AF', 'Afghanistan'),
(4, 'AL', 'Albania'),
(5, 'DZ', 'Algeria'),
(6, 'AS', 'American Samoa'),
(7, 'AD', 'Andorra'),
(8, 'AO', 'Angola'),
(9, 'AI', 'Anguilla'),
(10, 'AQ', 'Antarctica'),
(11, 'AG', 'Antigua and/or Barbuda'),
(12, 'AR', 'Argentina'),
(13, 'AM', 'Armenia'),
(14, 'AW', 'Aruba'),
(15, 'AU', 'Australia'),
(16, 'AT', 'Austria'),
(17, 'AZ', 'Azerbaijan'),
(18, 'BS', 'Bahamas'),
(19, 'BH', 'Bahrain'),
(20, 'BD', 'Bangladesh'),
(21, 'BB', 'Barbados'),
(22, 'BY', 'Belarus'),
(23, 'BE', 'Belgium'),
(24, 'BZ', 'Belize'),
(25, 'BJ', 'Benin'),
(26, 'BM', 'Bermuda'),
(27, 'BT', 'Bhutan'),
(28, 'BO', 'Bolivia'),
(29, 'BA', 'Bosnia and Herzegovina'),
(30, 'BW', 'Botswana'),
(31, 'BV', 'Bouvet Island'),
(32, 'BR', 'Brazil'),
(33, 'IO', 'British lndian Ocean Territory'),
(34, 'BN', 'Brunei Darussalam'),
(35, 'BG', 'Bulgaria'),
(36, 'BF', 'Burkina Faso'),
(37, 'BI', 'Burundi'),
(38, 'KH', 'Cambodia'),
(39, 'CM', 'Cameroon'),
(40, 'CV', 'Cape Verde'),
(41, 'KY', 'Cayman Islands'),
(42, 'CF', 'Central African Republic'),
(43, 'TD', 'Chad'),
(44, 'CL', 'Chile'),
(45, 'CN', 'China'),
(46, 'CX', 'Christmas Island'),
(47, 'CC', 'Cocos (Keeling) Islands'),
(48, 'CO', 'Colombia'),
(49, 'KM', 'Comoros'),
(50, 'CG', 'Congo'),
(51, 'CK', 'Cook Islands'),
(52, 'CR', 'Costa Rica'),
(53, 'HR', 'Croatia (Hrvatska)'),
(54, 'CU', 'Cuba'),
(55, 'CY', 'Cyprus'),
(56, 'CZ', 'Czech Republic'),
(57, 'CD', 'Democratic Republic of Congo'),
(58, 'DK', 'Denmark'),
(59, 'DJ', 'Djibouti'),
(60, 'DM', 'Dominica'),
(61, 'DO', 'Dominican Republic'),
(62, 'TP', 'East Timor'),
(63, 'EC', 'Ecudaor'),
(64, 'EG', 'Egypt'),
(65, 'SV', 'El Salvador'),
(66, 'GQ', 'Equatorial Guinea'),
(67, 'ER', 'Eritrea'),
(68, 'EE', 'Estonia'),
(69, 'ET', 'Ethiopia'),
(70, 'FK', 'Falkland Islands (Malvinas)'),
(71, 'FO', 'Faroe Islands'),
(72, 'FJ', 'Fiji'),
(73, 'FI', 'Finland'),
(74, 'FR', 'France'),
(75, 'FX', 'France, Metropolitan'),
(76, 'GF', 'French Guiana'),
(77, 'PF', 'French Polynesia'),
(78, 'TF', 'French Southern Territories'),
(79, 'GA', 'Gabon'),
(80, 'GM', 'Gambia'),
(81, 'GE', 'Georgia'),
(82, 'DE', 'Germany'),
(83, 'GH', 'Ghana'),
(84, 'GI', 'Gibraltar'),
(85, 'GR', 'Greece'),
(86, 'GL', 'Greenland'),
(87, 'GD', 'Grenada'),
(88, 'GP', 'Guadeloupe'),
(89, 'GU', 'Guam'),
(90, 'GT', 'Guatemala'),
(91, 'GN', 'Guinea'),
(92, 'GW', 'Guinea-Bissau'),
(93, 'GY', 'Guyana'),
(94, 'HT', 'Haiti'),
(95, 'HM', 'Heard and Mc Donald Islands'),
(96, 'HN', 'Honduras'),
(97, 'HK', 'Hong Kong'),
(98, 'HU', 'Hungary'),
(99, 'IS', 'Iceland'),
(100, 'IN', 'India'),
(101, 'ID', 'Indonesia'),
(102, 'IR', 'Iran (Islamic Republic of)'),
(103, 'IQ', 'Iraq'),
(104, 'IE', 'Ireland'),
(105, 'IL', 'Israel'),
(106, 'IT', 'Italy'),
(107, 'CI', 'Ivory Coast'),
(108, 'JM', 'Jamaica'),
(109, 'JP', 'Japan'),
(110, 'JO', 'Jordan'),
(111, 'KZ', 'Kazakhstan'),
(112, 'KE', 'Kenya'),
(113, 'KI', 'Kiribati'),
(114, 'KP', 'Korea, Democratic People\'s Republic of'),
(115, 'KR', 'Korea, Republic of'),
(116, 'KW', 'Kuwait'),
(117, 'KG', 'Kyrgyzstan'),
(118, 'LA', 'Lao People\'s Democratic Republic'),
(119, 'LV', 'Latvia'),
(120, 'LB', 'Lebanon'),
(121, 'LS', 'Lesotho'),
(122, 'LR', 'Liberia'),
(123, 'LY', 'Libyan Arab Jamahiriya'),
(124, 'LI', 'Liechtenstein'),
(125, 'LT', 'Lithuania'),
(126, 'LU', 'Luxembourg'),
(127, 'MO', 'Macau'),
(128, 'MK', 'Macedonia'),
(129, 'MG', 'Madagascar'),
(130, 'MW', 'Malawi'),
(131, 'MY', 'Malaysia'),
(132, 'MV', 'Maldives'),
(133, 'ML', 'Mali'),
(134, 'MT', 'Malta'),
(135, 'MH', 'Marshall Islands'),
(136, 'MQ', 'Martinique'),
(137, 'MR', 'Mauritania'),
(138, 'MU', 'Mauritius'),
(139, 'TY', 'Mayotte'),
(140, 'MX', 'Mexico'),
(141, 'FM', 'Micronesia, Federated States of'),
(142, 'MD', 'Moldova, Republic of'),
(143, 'MC', 'Monaco'),
(144, 'MN', 'Mongolia'),
(145, 'MS', 'Montserrat'),
(146, 'MA', 'Morocco'),
(147, 'MZ', 'Mozambique'),
(148, 'MM', 'Myanmar'),
(149, 'NA', 'Namibia'),
(150, 'NR', 'Nauru'),
(151, 'NP', 'Nepal'),
(152, 'NL', 'Netherlands'),
(153, 'AN', 'Netherlands Antilles'),
(154, 'NC', 'New Caledonia'),
(155, 'NZ', 'New Zealand'),
(156, 'NI', 'Nicaragua'),
(157, 'NE', 'Niger'),
(158, 'NG', 'Nigeria'),
(159, 'NU', 'Niue'),
(160, 'NF', 'Norfork Island'),
(161, 'MP', 'Northern Mariana Islands'),
(162, 'NO', 'Norway'),
(163, 'OM', 'Oman'),
(164, 'PK', 'Pakistan'),
(165, 'PW', 'Palau'),
(166, 'PA', 'Panama'),
(167, 'PG', 'Papua New Guinea'),
(168, 'PY', 'Paraguay'),
(169, 'PE', 'Peru'),
(170, 'PH', 'Philippines'),
(171, 'PN', 'Pitcairn'),
(172, 'PL', 'Poland'),
(173, 'PT', 'Portugal'),
(174, 'PR', 'Puerto Rico'),
(175, 'QA', 'Qatar'),
(176, 'SS', 'Republic of South Sudan'),
(177, 'RE', 'Reunion'),
(178, 'RO', 'Romania'),
(179, 'RU', 'Russian Federation'),
(180, 'RW', 'Rwanda'),
(181, 'KN', 'Saint Kitts and Nevis'),
(182, 'LC', 'Saint Lucia'),
(183, 'VC', 'Saint Vincent and the Grenadines'),
(184, 'WS', 'Samoa'),
(185, 'SM', 'San Marino'),
(186, 'ST', 'Sao Tome and Principe'),
(187, 'SA', 'Saudi Arabia'),
(188, 'SN', 'Senegal'),
(189, 'RS', 'Serbia'),
(190, 'SC', 'Seychelles'),
(191, 'SL', 'Sierra Leone'),
(192, 'SG', 'Singapore'),
(193, 'SK', 'Slovakia'),
(194, 'SI', 'Slovenia'),
(195, 'SB', 'Solomon Islands'),
(196, 'SO', 'Somalia'),
(197, 'ZA', 'South Africa'),
(198, 'GS', 'South Georgia South Sandwich Islands'),
(199, 'ES', 'Spain'),
(200, 'LK', 'Sri Lanka'),
(201, 'SH', 'St. Helena'),
(202, 'PM', 'St. Pierre and Miquelon'),
(203, 'SD', 'Sudan'),
(204, 'SR', 'Suriname'),
(205, 'SJ', 'Svalbarn and Jan Mayen Islands'),
(206, 'SZ', 'Swaziland'),
(207, 'SE', 'Sweden'),
(208, 'CH', 'Switzerland'),
(209, 'SY', 'Syrian Arab Republic'),
(210, 'TW', 'Taiwan'),
(211, 'TJ', 'Tajikistan'),
(212, 'TZ', 'Tanzania, United Republic of'),
(213, 'TH', 'Thailand'),
(214, 'TG', 'Togo'),
(215, 'TK', 'Tokelau'),
(216, 'TO', 'Tonga'),
(217, 'TT', 'Trinidad and Tobago'),
(218, 'TN', 'Tunisia'),
(219, 'TR', 'Turkey'),
(220, 'TM', 'Turkmenistan'),
(221, 'TC', 'Turks and Caicos Islands'),
(222, 'TV', 'Tuvalu'),
(223, 'UG', 'Uganda'),
(224, 'UA', 'Ukraine'),
(225, 'AE', 'United Arab Emirates'),
(226, 'GB', 'United Kingdom'),
(227, 'UM', 'United States minor outlying islands'),
(228, 'UY', 'Uruguay'),
(229, 'UZ', 'Uzbekistan'),
(230, 'VU', 'Vanuatu'),
(231, 'VA', 'Vatican City State'),
(232, 'VE', 'Venezuela'),
(233, 'VN', 'Vietnam'),
(234, 'VG', 'Virgin Islands (British)'),
(235, 'VI', 'Virgin Islands (U.S.)'),
(236, 'WF', 'Wallis and Futuna Islands'),
(237, 'EH', 'Western Sahara'),
(238, 'YE', 'Yemen'),
(239, 'YU', 'Yugoslavia'),
(240, 'ZR', 'Zaire'),
(241, 'ZM', 'Zambia'),
(242, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `couriers`
--

DROP TABLE IF EXISTS `couriers`;
CREATE TABLE IF NOT EXISTS `couriers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `couriers`
--

INSERT INTO `couriers` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'UPS', '2018-05-31 13:43:21', '2018-05-31 13:46:34', NULL),
(2, 'tyty', '2018-05-31 13:48:22', '2018-05-31 13:48:25', '2018-05-31 13:48:25'),
(3, 'Others', '2018-06-01 03:38:03', '2018-07-02 09:20:12', '2018-07-02 09:20:12'),
(4, 'dfdf2', '2018-07-02 09:20:16', '2018-07-02 09:20:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fabrics`
--

DROP TABLE IF EXISTS `fabrics`;
CREATE TABLE IF NOT EXISTS `fabrics` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `master_fabric_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fabrics`
--

INSERT INTO `fabrics` (`id`, `name`, `master_fabric_id`, `status`, `default`, `created_at`, `updated_at`, `deleted_at`) VALUES
(9, '100% COTTON', 5, 1, 0, '2018-05-12 11:02:30', '2018-06-30 09:57:35', NULL),
(10, 'er', 2, 1, 0, '2018-05-12 11:02:54', '2018-05-12 11:02:57', '2018-05-12 11:02:57'),
(8, '100% POLYAMIDE', 37, 1, 0, '2018-05-12 09:49:52', '2018-06-30 09:57:35', NULL),
(7, '53% COTTON 47% ACETATE', 5, 1, 0, '2018-05-12 09:49:28', '2018-06-30 09:57:35', NULL),
(6, '100% ACRYLIC', 43, 1, 0, '2018-05-12 09:48:02', '2018-06-30 09:57:35', NULL),
(11, 'dd2', 3, 1, 0, '2018-05-17 01:25:56', '2018-05-17 01:26:07', '2018-05-17 01:26:07'),
(12, '59% POLYESTER 35% POLYAMIDE 6% SPANDEX', 28, 1, 0, '2018-05-23 15:22:05', '2018-06-30 09:57:35', NULL),
(13, '60% COTTON 37% POLYAMIDE 3% SPANDEX', 3, 1, 0, '2018-05-23 15:22:16', '2018-06-30 09:57:35', NULL),
(14, '64% COTTON 36% POLYESTER', 5, 1, 0, '2018-05-23 15:22:25', '2018-06-30 09:57:35', NULL),
(15, '122', 3, 1, 1, '2018-05-26 11:42:54', '2018-05-26 12:03:45', '2018-05-26 12:03:45'),
(16, '3334', 43, 1, 1, '2018-06-30 09:55:59', '2018-06-30 09:57:39', '2018-06-30 09:57:39');

-- --------------------------------------------------------

--
-- Table structure for table `industries`
--

DROP TABLE IF EXISTS `industries`;
CREATE TABLE IF NOT EXISTS `industries` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `industries`
--

INSERT INTO `industries` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'erd', '2018-05-14 04:26:39', '2018-05-14 04:28:21', '2018-05-14 04:28:21'),
(2, 'df', '2018-05-14 04:27:04', '2018-05-14 05:14:22', '2018-05-14 05:14:22'),
(3, 'Women\'s Clothing', '2018-05-14 05:14:32', '2018-05-14 05:14:32', NULL),
(4, 'Men\'s Clothing', '2018-05-14 05:14:44', '2018-05-14 05:14:44', NULL),
(5, 'Children\'s Clothings', '2018-05-14 05:14:59', '2018-05-14 05:14:59', NULL),
(6, 'Accessories', '2018-05-14 05:15:09', '2018-05-14 05:15:09', NULL),
(7, 'Shoes', '2018-05-14 05:15:19', '2018-05-14 05:15:19', NULL),
(8, 'Fixure', '2018-05-14 05:15:28', '2018-05-14 05:15:28', NULL),
(9, 'Handbag', '2018-05-14 05:15:38', '2018-05-14 05:15:38', NULL),
(10, 'Cosmetic', '2018-05-14 05:15:47', '2018-05-14 05:15:47', NULL),
(11, 'Lingerie', '2018-05-14 05:15:58', '2018-05-14 05:15:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `industry_user`
--

DROP TABLE IF EXISTS `industry_user`;
CREATE TABLE IF NOT EXISTS `industry_user` (
  `user_id` int(11) NOT NULL,
  `industry_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `industry_user`
--

INSERT INTO `industry_user` (`user_id`, `industry_id`) VALUES
(1, 3),
(1, 5),
(1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

DROP TABLE IF EXISTS `items`;
CREATE TABLE IF NOT EXISTS `items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL,
  `style_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `orig_price` double(8,2) DEFAULT NULL,
  `pack_id` int(11) NOT NULL,
  `sorting` int(11) DEFAULT NULL,
  `description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `available_on` date DEFAULT NULL,
  `availability` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_parent_category` int(11) NOT NULL,
  `default_second_category` int(11) DEFAULT NULL,
  `default_third_category` int(11) DEFAULT NULL,
  `min_qty` int(11) DEFAULT NULL,
  `fabric` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `made_in_id` int(11) DEFAULT NULL,
  `labeled` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `memo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `status`, `style_no`, `price`, `orig_price`, `pack_id`, `sorting`, `description`, `available_on`, `availability`, `name`, `default_parent_category`, `default_second_category`, `default_third_category`, `min_qty`, `fabric`, `made_in_id`, `labeled`, `memo`, `activated_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 0, 'TEST!2', 12.00, NULL, 4, 5, NULL, '2018-07-04', 1, 'rerer', 1, 10, 17, 6, NULL, 1, 'labeled', NULL, '2018-07-02 07:32:31', '2018-06-30 11:21:50', '2018-07-09 07:45:09', NULL),
(3, 0, 'TEST!2-Clone-delete-103831705', 12.00, NULL, 4, 5, NULL, '2018-07-04', NULL, 'rerer', 1, 10, 17, 6, NULL, 1, 'labeled', NULL, NULL, '2018-06-30 12:15:27', '2018-06-30 12:16:50', '2018-06-30 12:16:50'),
(4, 0, 'TEST!2-Clone', 12.00, NULL, 4, 5, NULL, '2018-07-04', 1, 'rerer', 62, NULL, NULL, 6, NULL, 1, 'labeled', NULL, '2018-07-02 07:32:31', '2018-06-30 12:17:00', '2018-07-09 07:45:09', NULL),
(5, 1, 'N01', 12.00, 15.00, 1, NULL, NULL, NULL, 2, 'Flat Metallic Cut Disc Pendant Necklace', 1, 10, 12, 6, NULL, 18, 'labeled', NULL, NULL, '2018-07-09 07:51:06', '2018-07-09 07:51:06', NULL),
(6, 1, 'N343', 54.00, NULL, 1, NULL, NULL, NULL, 2, 'Open Raindrop Netted Bead Pendant Necklace', 1, 10, 12, NULL, NULL, NULL, NULL, NULL, '2018-07-09 07:53:42', '2018-07-09 07:53:38', '2018-07-09 07:53:42', NULL),
(7, 1, 'N734', 67.00, NULL, 1, NULL, NULL, NULL, 2, 'Tiered Ring Triangle Rhinestone Charm Necklace', 1, 10, 13, NULL, NULL, NULL, NULL, NULL, '2018-07-09 07:56:59', '2018-07-09 07:56:56', '2018-07-09 07:56:59', NULL),
(8, 1, 'N67', 34.00, NULL, 1, NULL, NULL, NULL, 2, 'Scorpio Pendant Necklace.', 1, 10, 13, NULL, NULL, NULL, NULL, NULL, '2018-07-09 07:58:51', '2018-07-09 07:58:48', '2018-07-09 07:58:51', NULL),
(9, 1, 'N546', 45.00, NULL, 1, NULL, NULL, NULL, 2, 'Layered Metallic Warped Circle Pendant Necklace', 1, 10, 17, NULL, NULL, NULL, NULL, NULL, '2018-07-09 08:00:36', '2018-07-09 08:00:33', '2018-07-09 08:00:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_images`
--

DROP TABLE IF EXISTS `item_images`;
CREATE TABLE IF NOT EXISTS `item_images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `list_image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbs_image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item_images`
--

INSERT INTO `item_images` (`id`, `item_id`, `color_id`, `sort`, `image_path`, `list_image_path`, `thumbs_image_path`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, '/images/item/6e54eed0-7c88-11e8-97da-8ff2bf00b99c.jpg', NULL, NULL, '2018-06-30 11:10:04', '2018-06-30 11:10:04'),
(2, 2, NULL, 1, 'images/item/original/134dc620-7c8a-11e8-a65a-bd1ab6a8cc80.jpg', 'images/item/list/134dc620-7c8a-11e8-a65a-bd1ab6a8cc80.jpg', 'images/item/thumbs/134dc620-7c8a-11e8-a65a-bd1ab6a8cc80.jpg', '2018-06-30 11:18:16', '2018-06-30 12:02:10'),
(3, NULL, NULL, NULL, '/images/item/31fda5b0-7c91-11e8-9394-19058d004339.jpg', NULL, NULL, '2018-06-30 12:12:48', '2018-06-30 12:12:48'),
(4, 4, NULL, 1, 'images/item/original/c847f460-7c91-11e8-9d46-e1ed8ac6a1cb.jpg', 'images/item/list/c847f460-7c91-11e8-9d46-e1ed8ac6a1cb.jpg', 'images/item/thumbs/c847f460-7c91-11e8-9d46-e1ed8ac6a1cb.jpg', '2018-06-30 12:16:59', '2018-06-30 13:23:36'),
(5, 4, NULL, 2, 'images/item/original/c83a3350-7c91-11e8-b570-256c2bff4f54.jpg', 'images/item/list/c83a3350-7c91-11e8-b570-256c2bff4f54.jpg', 'images/item/thumbs/c83a3350-7c91-11e8-b570-256c2bff4f54.jpg', '2018-06-30 12:17:00', '2018-06-30 13:23:36'),
(6, 5, NULL, 2, 'images/item/original/20f589e0-837f-11e8-a7c5-87faaadd99b5.jpg', 'images/item/list/20f589e0-837f-11e8-a7c5-87faaadd99b5.jpg', 'images/item/thumbs/20f589e0-837f-11e8-a7c5-87faaadd99b5.jpg', '2018-07-09 07:49:24', '2018-07-09 07:51:07'),
(7, 5, NULL, 3, 'images/item/original/21027570-837f-11e8-965c-db4640c07abe.jpg', 'images/item/list/21027570-837f-11e8-965c-db4640c07abe.jpg', 'images/item/thumbs/21027570-837f-11e8-965c-db4640c07abe.jpg', '2018-07-09 07:49:24', '2018-07-09 07:51:07'),
(8, 5, NULL, 1, 'images/item/original/20cb0000-837f-11e8-a8d5-e748b2753061.jpg', 'images/item/list/20cb0000-837f-11e8-a8d5-e748b2753061.jpg', 'images/item/thumbs/20cb0000-837f-11e8-a8d5-e748b2753061.jpg', '2018-07-09 07:49:24', '2018-07-09 07:51:07'),
(9, 6, NULL, 1, 'images/item/original/7add8160-837f-11e8-b217-cf67dc56314c.jpg', 'images/item/list/7add8160-837f-11e8-b217-cf67dc56314c.jpg', 'images/item/thumbs/7add8160-837f-11e8-b217-cf67dc56314c.jpg', '2018-07-09 07:53:33', '2018-07-09 07:53:38'),
(10, 6, NULL, 3, 'images/item/original/7af96890-837f-11e8-b63c-9b3581e71c65.jpg', 'images/item/list/7af96890-837f-11e8-b63c-9b3581e71c65.jpg', 'images/item/thumbs/7af96890-837f-11e8-b63c-9b3581e71c65.jpg', '2018-07-09 07:53:33', '2018-07-09 07:53:38'),
(11, 6, NULL, 2, 'images/item/original/7aebcb90-837f-11e8-a3ec-1559201f0f12.jpg', 'images/item/list/7aebcb90-837f-11e8-a3ec-1559201f0f12.jpg', 'images/item/thumbs/7aebcb90-837f-11e8-a3ec-1559201f0f12.jpg', '2018-07-09 07:53:33', '2018-07-09 07:53:38'),
(12, 7, NULL, 1, 'images/item/original/f0e88620-837f-11e8-b2e3-cf28f7bef775.jpg', 'images/item/list/f0e88620-837f-11e8-b2e3-cf28f7bef775.jpg', 'images/item/thumbs/f0e88620-837f-11e8-b2e3-cf28f7bef775.jpg', '2018-07-09 07:56:40', '2018-07-09 07:56:56'),
(13, 7, NULL, 2, 'images/item/original/f0f679e0-837f-11e8-8db8-3fa4e76c86ce.jpg', 'images/item/list/f0f679e0-837f-11e8-8db8-3fa4e76c86ce.jpg', 'images/item/thumbs/f0f679e0-837f-11e8-8db8-3fa4e76c86ce.jpg', '2018-07-09 07:56:40', '2018-07-09 07:56:56'),
(14, 8, NULL, 2, 'images/item/original/33f764d0-8380-11e8-87d0-598a1ba9e8e5.JPG', 'images/item/list/33f764d0-8380-11e8-87d0-598a1ba9e8e5.JPG', 'images/item/thumbs/33f764d0-8380-11e8-87d0-598a1ba9e8e5.JPG', '2018-07-09 07:58:40', '2018-07-09 07:58:48'),
(15, 8, NULL, 1, 'images/item/original/33ebf580-8380-11e8-902e-ff4bf2c75022.jpg', 'images/item/list/33ebf580-8380-11e8-902e-ff4bf2c75022.jpg', 'images/item/thumbs/33ebf580-8380-11e8-902e-ff4bf2c75022.jpg', '2018-07-09 07:58:40', '2018-07-09 07:58:48'),
(16, 9, NULL, 3, 'images/item/original/72712d80-8380-11e8-a788-11435879e048.jpg', 'images/item/list/72712d80-8380-11e8-a788-11435879e048.jpg', 'images/item/thumbs/72712d80-8380-11e8-a788-11435879e048.jpg', '2018-07-09 08:00:14', '2018-07-09 08:00:33'),
(17, 9, NULL, 1, 'images/item/original/72564870-8380-11e8-8d4d-f1eb6da87927.jpg', 'images/item/list/72564870-8380-11e8-8d4d-f1eb6da87927.jpg', 'images/item/thumbs/72564870-8380-11e8-8d4d-f1eb6da87927.jpg', '2018-07-09 08:00:14', '2018-07-09 08:00:33'),
(18, 9, NULL, 2, 'images/item/original/7263c820-8380-11e8-b7fb-6bc07d4cdf0a.jpg', 'images/item/list/7263c820-8380-11e8-b7fb-6bc07d4cdf0a.jpg', 'images/item/thumbs/7263c820-8380-11e8-b7fb-6bc07d4cdf0a.jpg', '2018-07-09 08:00:14', '2018-07-09 08:00:33');

-- --------------------------------------------------------

--
-- Table structure for table `lengths`
--

DROP TABLE IF EXISTS `lengths`;
CREATE TABLE IF NOT EXISTS `lengths` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lengths`
--

INSERT INTO `lengths` (`id`, `name`, `sub_category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'sdf2', 8, '2018-05-17 07:02:23', '2018-05-17 07:02:52', '2018-05-17 07:02:52'),
(2, '1/2 Sleeve', 10, '2018-05-17 07:02:50', '2018-05-22 14:23:02', NULL),
(3, '3/4 Sleeve', 10, '2018-05-22 14:23:10', '2018-05-22 14:23:10', NULL),
(4, 'Asymmetrical', 10, '2018-05-22 14:23:18', '2018-05-22 14:23:18', NULL),
(5, 'Bell Sleeves', 10, '2018-05-22 14:23:24', '2018-05-22 14:23:24', NULL),
(6, 'Bubble Sleeve', 10, '2018-05-22 14:23:32', '2018-05-22 14:23:32', NULL),
(7, 'Cap Sleeve', 10, '2018-05-22 14:23:42', '2018-05-22 14:23:42', NULL),
(8, 'Dolman Sleeve', 10, '2018-05-22 14:23:49', '2018-05-22 14:23:49', NULL),
(9, 'Flyaway Sleeve', 10, '2018-05-22 14:23:57', '2018-05-22 14:23:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `login_history`
--

DROP TABLE IF EXISTS `login_history`;
CREATE TABLE IF NOT EXISTS `login_history` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `login_history`
--

INSERT INTO `login_history` (`id`, `user_id`, `ip`, `created_at`, `updated_at`) VALUES
(1, 1, '::1', '2018-06-29 08:55:23', '2018-06-29 08:55:23'),
(2, 1, '::1', '2018-06-29 08:59:50', '2018-06-29 08:59:50'),
(3, 1, '::1', '2018-06-29 08:59:59', '2018-06-29 08:59:59'),
(4, 1, '::1', '2018-06-29 09:11:44', '2018-06-29 09:11:44'),
(5, 1, '::1', '2018-06-29 09:13:20', '2018-06-29 09:13:20'),
(6, 1, '::1', '2018-06-29 10:22:24', '2018-06-29 10:22:24'),
(7, 1, '::1', '2018-06-30 06:50:22', '2018-06-30 06:50:22'),
(8, 1, '::1', '2018-06-30 08:50:24', '2018-06-30 08:50:24'),
(9, 1, '::1', '2018-07-02 06:34:01', '2018-07-02 06:34:01'),
(10, 1, '::1', '2018-07-02 06:45:09', '2018-07-02 06:45:09'),
(11, 1, '::1', '2018-07-04 06:01:20', '2018-07-04 06:01:20'),
(12, 1, '::1', '2018-07-04 11:21:19', '2018-07-04 11:21:19'),
(13, 1, '::1', '2018-07-05 02:34:59', '2018-07-05 02:34:59'),
(14, 1, '::1', '2018-07-05 12:38:17', '2018-07-05 12:38:17'),
(15, 1, '::1', '2018-07-07 08:55:41', '2018-07-07 08:55:41'),
(16, 1, '::1', '2018-07-07 09:58:47', '2018-07-07 09:58:47'),
(17, 1, '::1', '2018-07-09 04:17:43', '2018-07-09 04:17:43'),
(18, 1, '::1', '2018-07-09 07:27:05', '2018-07-09 07:27:05'),
(19, 1, '::1', '2018-07-09 08:47:27', '2018-07-09 08:47:27'),
(20, 1, '::1', '2018-07-09 12:58:27', '2018-07-09 12:58:27'),
(21, 14, '::1', '2018-07-12 08:16:22', '2018-07-12 08:16:22'),
(22, 14, '::1', '2018-07-12 08:44:00', '2018-07-12 08:44:00'),
(23, 14, '::1', '2018-07-12 08:44:15', '2018-07-12 08:44:15'),
(24, 14, '::1', '2018-07-12 09:03:18', '2018-07-12 09:03:18'),
(25, 14, '::1', '2018-07-12 09:11:24', '2018-07-12 09:11:24'),
(26, 14, '::1', '2018-07-12 12:03:27', '2018-07-12 12:03:27'),
(27, 14, '::1', '2018-07-13 07:09:23', '2018-07-13 07:09:23'),
(28, 1, '::1', '2018-07-13 09:18:56', '2018-07-13 09:18:56');

-- --------------------------------------------------------

--
-- Table structure for table `made_in_countries`
--

DROP TABLE IF EXISTS `made_in_countries`;
CREATE TABLE IF NOT EXISTS `made_in_countries` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `made_in_countries`
--

INSERT INTO `made_in_countries` (`id`, `name`, `status`, `default`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'IMPORT', 1, 1, '2018-05-12 05:03:14', '2018-06-30 09:57:51', NULL),
(11, 'ee', 1, 0, '2018-05-12 06:08:15', '2018-05-12 06:28:46', '2018-05-12 06:28:46'),
(12, 'd', 0, 0, '2018-05-12 06:20:36', '2018-05-22 14:43:53', '2018-05-22 14:43:53'),
(13, '2', 0, 0, '2018-05-12 06:43:58', '2018-05-12 06:44:11', '2018-05-12 06:44:11'),
(8, '4', 1, 0, '2018-05-12 05:42:22', '2018-05-22 14:43:49', '2018-05-22 14:43:49'),
(10, 'aa', 1, 0, '2018-05-12 05:46:40', '2018-05-22 14:43:51', '2018-05-22 14:43:51'),
(14, 'dfd', 1, 0, '2018-05-12 09:27:19', '2018-05-22 14:43:54', '2018-05-22 14:43:54'),
(15, 'dda3', 0, 0, '2018-05-17 01:30:11', '2018-05-17 01:30:28', '2018-05-17 01:30:28'),
(16, 'USA', 1, 0, '2018-05-22 14:44:10', '2018-06-30 09:57:51', NULL),
(17, 'IMPORT', 1, 0, '2018-05-23 15:22:38', '2018-06-30 09:57:51', NULL),
(18, 'USA', 1, 0, '2018-05-23 15:22:47', '2018-06-30 09:57:51', NULL),
(19, 'd', 1, 0, '2018-06-25 08:01:29', '2018-06-25 08:01:33', '2018-06-25 08:01:33'),
(20, 'erer', 0, 0, '2018-06-30 09:57:44', '2018-06-30 09:57:56', '2018-06-30 09:57:56');

-- --------------------------------------------------------

--
-- Table structure for table `master_colors`
--

DROP TABLE IF EXISTS `master_colors`;
CREATE TABLE IF NOT EXISTS `master_colors` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_colors`
--

INSERT INTO `master_colors` (`id`, `name`, `image_path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(8, 'Black', '/images/master_color/4e77ad10-5df2-11e8-9866-35f0a9aad927.jpg', '2018-05-22 12:59:51', '2018-05-22 12:59:51', NULL),
(7, 'Red', '/images/master_color/3586a0b0-5df2-11e8-8423-fbb0e7848eaf.jpg', '2018-05-22 12:59:09', '2018-05-22 12:59:09', NULL),
(9, 'Blue', '/images/master_color/5e75f940-5df2-11e8-8b31-53e1d9dda377.jpg', '2018-05-22 13:00:18', '2018-05-22 13:00:18', NULL),
(10, 'Brown', '/images/master_color/791e7f20-5df2-11e8-b6ea-f9806acbee22.jpg', '2018-05-22 13:01:03', '2018-05-22 13:01:03', NULL),
(11, 'Gold', '/images/master_color/871c7b10-5df2-11e8-bd06-5d3d96e22977.jpg', '2018-05-22 13:01:26', '2018-05-22 13:01:26', NULL),
(12, 'Gray', '/images/master_color/97251680-5df2-11e8-80d0-0b45ed04fbf9.jpg', '2018-05-22 13:01:53', '2018-05-22 13:01:53', NULL),
(13, 'Green', '/images/master_color/ad0764b0-5df2-11e8-bf69-49778d6fff9a.jpg', '2018-05-22 13:02:30', '2018-05-22 13:02:30', NULL),
(14, 'Ivory', '/images/master_color/c1f2e920-5df2-11e8-8da5-5b7f0944a348.jpg', '2018-05-22 13:03:05', '2018-05-22 13:03:05', NULL),
(15, 'Multi', '/images/master_color/d3727d00-5df2-11e8-8efd-e3a8ede6be2e.jpg', '2018-05-22 13:03:34', '2018-05-22 13:03:34', NULL),
(16, 'Orange', '/images/master_color/e5b44320-5df2-11e8-8331-275f5efbdb75.jpg', '2018-05-22 13:04:05', '2018-05-22 13:04:05', NULL),
(17, 'Pink', '/images/master_color/f9303bb0-5df2-11e8-a50f-230282c65397.jpg', '2018-05-22 13:04:37', '2018-05-22 13:04:37', NULL),
(18, 'Purple', '/images/master_color/12f03b80-5df3-11e8-a134-07210ad6c7cb.jpg', '2018-05-22 13:05:21', '2018-05-22 13:05:21', NULL),
(19, 'Silver', '/images/master_color/283ad410-5df3-11e8-878c-15abac44d755.jpg', '2018-05-22 13:05:56', '2018-05-22 13:05:56', NULL),
(20, 'Beige', '/images/master_color/3bbf2f50-5df3-11e8-861a-ff67af98860e.jpg', '2018-05-22 13:06:29', '2018-05-22 13:06:29', NULL),
(21, 'White', '/images/master_color/4b694870-5df3-11e8-a72e-7702ab347774.jpg', '2018-05-22 13:06:55', '2018-05-22 13:06:55', NULL),
(22, 'Yellow', '/images/master_color/5b5a3520-5df3-11e8-a6bc-bd02725fcab8.jpg', '2018-05-22 13:07:22', '2018-06-29 09:43:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `master_fabrics`
--

DROP TABLE IF EXISTS `master_fabrics`;
CREATE TABLE IF NOT EXISTS `master_fabrics` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_fabrics`
--

INSERT INTO `master_fabrics` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Canvas', '2018-05-12 08:04:56', '2018-05-22 13:10:03', NULL),
(2, 'Chiffon', '2018-05-12 08:05:13', '2018-05-22 13:10:12', NULL),
(3, 'Acid Wash', '2018-05-12 08:08:01', '2018-05-22 13:09:48', NULL),
(4, 'Corduroy', '2018-05-22 13:10:23', '2018-05-22 13:10:23', NULL),
(5, 'Cotton', '2018-05-22 13:10:32', '2018-05-22 13:10:32', NULL),
(6, 'Crystal', '2018-05-22 13:10:37', '2018-05-22 13:10:37', NULL),
(7, 'Dark Wash', '2018-05-22 13:10:49', '2018-05-22 13:10:49', NULL),
(8, 'Denim', '2018-05-22 13:11:21', '2018-05-22 13:11:21', NULL),
(9, 'Faux Leather', '2018-05-22 13:11:27', '2018-05-22 13:11:27', NULL),
(10, 'Faux Pearl', '2018-05-22 13:11:32', '2018-05-22 13:11:32', NULL),
(11, 'Genuine Stones', '2018-05-22 13:11:38', '2018-05-22 13:11:38', NULL),
(12, 'Jersey', '2018-05-22 13:11:43', '2018-05-22 13:11:43', NULL),
(13, 'Lace', '2018-05-22 13:11:49', '2018-05-22 13:11:49', NULL),
(14, 'Leather', '2018-05-22 13:12:06', '2018-05-22 13:12:06', NULL),
(15, 'Leather/Pleather', '2018-05-22 13:12:11', '2018-05-22 13:12:11', NULL),
(16, 'Light Wash', '2018-05-22 13:12:18', '2018-05-22 13:12:18', NULL),
(17, 'Linen', '2018-05-22 13:12:23', '2018-05-22 13:12:23', NULL),
(18, 'Mesh', '2018-05-22 13:12:28', '2018-05-22 13:12:28', NULL),
(19, 'Metal', '2018-05-22 13:12:34', '2018-05-22 13:12:34', NULL),
(20, 'Patent Leather', '2018-05-22 13:12:38', '2018-05-22 13:12:38', NULL),
(21, 'Pearl', '2018-05-22 13:12:44', '2018-05-22 13:12:44', NULL),
(22, 'Plastic', '2018-05-22 13:12:51', '2018-05-22 13:12:51', NULL),
(23, 'Pleather', '2018-05-22 13:12:55', '2018-05-22 13:12:55', NULL),
(24, 'Polyester', '2018-05-22 13:12:59', '2018-05-22 13:12:59', NULL),
(25, 'Rhinestones', '2018-05-22 13:13:04', '2018-05-22 13:13:04', NULL),
(26, 'Satin', '2018-05-22 13:13:08', '2018-05-22 13:13:08', NULL),
(27, 'Silk', '2018-05-22 13:13:15', '2018-05-22 13:13:15', NULL),
(28, 'Spandex', '2018-05-22 13:13:23', '2018-05-22 13:13:23', NULL),
(29, 'Stones', '2018-05-22 13:13:28', '2018-05-22 13:13:28', NULL),
(30, 'Straw', '2018-05-22 13:13:33', '2018-05-22 13:13:33', NULL),
(31, 'Suede', '2018-05-22 13:13:37', '2018-05-22 13:13:37', NULL),
(32, 'Taffeta', '2018-05-22 13:13:43', '2018-05-22 13:13:43', NULL),
(33, 'Terry/Velour', '2018-05-22 13:13:47', '2018-05-22 13:13:47', NULL),
(34, 'Wood', '2018-05-22 13:13:55', '2018-05-22 13:13:55', NULL),
(35, 'Wool', '2018-05-22 13:14:01', '2018-05-22 13:14:01', NULL),
(36, 'Rayon', '2018-05-22 13:14:06', '2018-05-22 13:14:06', NULL),
(37, 'Cotton/Poly', '2018-05-22 13:14:10', '2018-05-22 13:14:10', NULL),
(38, 'Viscose', '2018-05-22 13:14:17', '2018-05-22 13:14:17', NULL),
(39, 'Nylon', '2018-05-22 13:14:22', '2018-05-22 13:14:22', NULL),
(40, 'Medium Wash', '2018-05-22 13:14:27', '2018-05-22 13:14:27', NULL),
(41, 'White Denim', '2018-05-22 13:14:31', '2018-05-22 13:14:31', NULL),
(42, 'Black and Grey', '2018-05-22 13:14:36', '2018-05-22 13:14:36', NULL),
(43, 'Acrylic', '2018-05-22 13:14:42', '2018-05-22 13:14:42', NULL),
(44, 'Tencel', '2018-05-22 13:14:53', '2018-05-22 13:14:53', NULL),
(45, 'Cashmere', '2018-05-22 13:14:57', '2018-05-22 13:14:57', NULL),
(46, 'Modal', '2018-05-22 13:15:01', '2018-05-22 13:15:01', NULL),
(47, 'Velvet', '2018-05-22 13:15:06', '2018-05-22 13:15:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `meta_buyers`
--

DROP TABLE IF EXISTS `meta_buyers`;
CREATE TABLE IF NOT EXISTS `meta_buyers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `verified` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `block` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `primary_customer_market` int(11) NOT NULL,
  `seller_permit_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sell_online` tinyint(1) DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attention` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_state_id` int(11) DEFAULT NULL,
  `billing_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_country_id` int(11) NOT NULL,
  `billing_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_commercial` tinyint(1) DEFAULT NULL,
  `hear_about_us` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hear_about_us_other` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receive_offers` tinyint(1) NOT NULL,
  `sales2_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sales1_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ein_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meta_buyers`
--

INSERT INTO `meta_buyers` (`id`, `verified`, `active`, `block`, `user_id`, `company_name`, `primary_customer_market`, `seller_permit_number`, `sell_online`, `website`, `attention`, `billing_location`, `billing_address`, `billing_unit`, `billing_city`, `billing_state_id`, `billing_state`, `billing_zip`, `billing_country_id`, `billing_phone`, `billing_fax`, `billing_commercial`, `hear_about_us`, `hear_about_us_other`, `receive_offers`, `sales2_path`, `sales1_path`, `ein_path`, `created_at`, `updated_at`) VALUES
(3, 1, 1, 0, 14, 'Buyer LTD', 1, '54654654', NULL, NULL, NULL, 'US', 'Raj', NULL, 'gsdfgfga', 10, NULL, '5656', 1, '4576767', '4545', 0, 'bing', NULL, 1, NULL, NULL, NULL, '2018-05-24 10:22:34', '2018-06-04 11:51:11'),
(4, 1, 1, 0, 19, 'newer', 2, '343434', 0, NULL, '53453', 'US', '45345', NULL, '45', 8, NULL, '34535', 1, '4545', NULL, 0, 'yahoo', NULL, 1, NULL, NULL, NULL, '2018-06-22 07:18:27', '2018-06-22 07:18:27'),
(5, 0, 0, 0, 20, 'Buyer File LTD', 1, '34342', NULL, NULL, NULL, 'US', 'eqrwer', NULL, 'qewrqew', 8, NULL, 'weqrqwe', 1, '342423', NULL, 0, 'yahoo', NULL, 1, '/files/buyer/5cc184d0-7c61-11e8-965f-cb774fc2e7cb.pdf', '/files/buyer/5cc15790-7c61-11e8-9614-a51810cf5135.jpg', '/files/buyer/5cc118a0-7c61-11e8-8567-f52e793c25fa.jpg', '2018-06-30 06:30:24', '2018-06-30 06:30:24'),
(6, 0, 0, 0, 21, 'Yuyer', 1, '234234', NULL, NULL, 'qwer', 'US', 'qwer', NULL, 'qwer', 4, NULL, 'qwer', 1, 'qwer', 'qwer', 0, 'bing', NULL, 1, '/files/buyer/b4c18fd0-85d7-11e8-a706-33ed54185cf1.jpg', '/files/buyer/b4c11080-85d7-11e8-af87-ebff915ff2d9.jpg', '/files/buyer/b4c0d420-85d7-11e8-9a75-9f841eed0c8b.jpg', '2018-07-12 07:30:13', '2018-07-12 07:30:13'),
(7, 0, 0, 0, 22, 'Fame Test', 1, '54545', NULL, NULL, 'asdf', 'US', 'asdf', NULL, 'asdf', 3, NULL, 'asdf', 1, 'aasdf', 'asdf', 0, 'other_search', NULL, 1, '/files/buyer/89aa54a0-85db-11e8-b2d6-bbcf3b5885f9.jpg', '/files/buyer/89aa2a50-85db-11e8-bab6-434af532ddfe.jpg', '/files/buyer/89a9f580-85db-11e8-bbaf-5d81846953d8.jpg', '2018-07-12 07:57:38', '2018-07-12 07:57:38');

-- --------------------------------------------------------

--
-- Table structure for table `meta_vendors`
--

DROP TABLE IF EXISTS `meta_vendors`;
CREATE TABLE IF NOT EXISTS `meta_vendors` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `verified` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `business_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_state_id` int(11) DEFAULT NULL,
  `billing_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_country_id` int(11) NOT NULL,
  `billing_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `billing_alternate_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_commercial` tinyint(1) NOT NULL,
  `factory_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_unit` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_state_id` int(11) DEFAULT NULL,
  `factory_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_zip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_country_id` int(11) NOT NULL,
  `factory_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `factory_alternate_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_evening_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `factory_commercial` tinyint(1) DEFAULT NULL,
  `company_info` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hear_about_us` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hear_about_us_other` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_notice` varchar(3000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size_chart` varchar(3000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receive_offers` tinyint(1) NOT NULL,
  `setting_estimated_shipping_charge` tinyint(1) NOT NULL DEFAULT '0',
  `setting_consolidation` tinyint(1) NOT NULL DEFAULT '0',
  `setting_sort_activation_date` tinyint(1) NOT NULL DEFAULT '0',
  `setting_unverified_checkout` tinyint(1) NOT NULL DEFAULT '0',
  `setting_unverified_user` tinyint(1) NOT NULL DEFAULT '0',
  `setting_not_logged` tinyint(1) NOT NULL DEFAULT '0',
  `show_in_main_slider` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `meta_vendors`
--

INSERT INTO `meta_vendors` (`id`, `verified`, `active`, `company_name`, `business_name`, `website`, `billing_location`, `billing_address`, `billing_unit`, `billing_city`, `billing_state_id`, `billing_state`, `billing_zip`, `billing_country_id`, `billing_phone`, `billing_alternate_phone`, `billing_fax`, `billing_commercial`, `factory_location`, `factory_address`, `factory_unit`, `factory_city`, `factory_state_id`, `factory_state`, `factory_zip`, `factory_country_id`, `factory_phone`, `factory_alternate_phone`, `factory_evening_phone`, `factory_fax`, `factory_commercial`, `company_info`, `hear_about_us`, `hear_about_us_other`, `order_notice`, `size_chart`, `receive_offers`, `setting_estimated_shipping_charge`, `setting_consolidation`, `setting_sort_activation_date`, `setting_unverified_checkout`, `setting_unverified_user`, `setting_not_logged`, `show_in_main_slider`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Binjar', 'Binjar Inc.', 'www.binjar.com', 'CA', 'Banashree', '2', 'Dhaka', 68, 'Rampura', '1219', 2, '01670985422', NULL, '123456', 1, 'US', 'cuet', '2', 'Chittagong', 11, 'Raozan', '3152', 1, '123456789', '45664', NULL, '4555', NULL, 'Company Info in details2', 'other', 'Bola jabena', '<p>&nbsp;asdf<strong> asd ORDER6</strong></p>', '<p>&nbsp;asdfa<em> ds2</em></p>', 1, 0, 0, 1, 0, 0, 0, 1, '2018-05-08 09:03:20', '2018-07-02 09:05:15');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=159 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(46, '2014_10_12_000000_create_users_table', 1),
(47, '2014_10_12_100000_create_password_resets_table', 1),
(48, '2018_05_05_133752_create_default_categories_table', 1),
(49, '2018_05_08_080401_create_countries_table', 1),
(50, '2018_05_08_083857_create_states_table', 1),
(51, '2018_05_08_134438_create_meta_vendors_table', 1),
(52, '2018_05_09_103818_create_sizes_table', 2),
(53, '2018_05_11_105225_create_packs_table', 3),
(54, '2018_05_11_120421_create_master_colors_table', 4),
(56, '2018_05_11_135750_create_colors_table', 5),
(57, '2018_05_12_103732_create_made_in_countries_table', 6),
(58, '2018_05_12_134524_create_master_fabrics_table', 7),
(59, '2018_05_12_151609_create_fabrics_table', 8),
(60, '2018_05_14_101752_create_industries_table', 9),
(62, '2018_05_14_122651_add_year_establish_in_meta_vendors_table', 10),
(63, '2018_05_14_133652_create_industry_user_table', 11),
(64, '2018_05_14_152938_add_size_chart_column_in_meta_vendors_table', 12),
(65, '2018_05_15_092930_add_sort_column_in_default_categories_table', 13),
(66, '2018_05_15_110154_add_user_id_column_in_users_table', 14),
(67, '2018_05_15_110903_remove_user_id_column_in_meta_vendors_table', 15),
(68, '2018_05_15_125606_add_status_column_in_users_table', 16),
(69, '2018_05_15_131551_add_last_login_column_in_users_table', 17),
(70, '2018_05_15_135817_remove_verified_column_in_users_table', 18),
(71, '2018_05_15_135945_add_verified_column_in_meta_vendors_table', 19),
(72, '2018_05_15_164157_create_user_permission_table', 20),
(73, '2018_05_16_081643_add_settings_columns_in_meta_vendors_table', 21),
(74, '2018_05_16_091300_create_login_history_table', 22),
(79, '2018_05_16_121659_create_categories_table', 23),
(80, '2018_05_16_173514_change_user_id_to_meta_vendors_in_packs_table', 24),
(81, '2018_05_17_072246_change_user_id_to_meta_vendor_id_in_fabrics_table', 25),
(82, '2018_05_17_072801_change_user_id_to_meta_vendor_id_in_made_in_countries_table', 26),
(83, '2018_05_17_084204_create_body_sizes_table', 27),
(84, '2018_05_17_113905_create_patterns_table', 28),
(85, '2018_05_17_124941_create_lengths_table', 29),
(86, '2018_05_17_133027_create_styles_table', 30),
(87, '2018_05_17_163110_change_user_id_to_meta_vendor_id_in_made_in_colors_table', 31),
(88, '2018_05_18_142050_create_item_images_table', 32),
(89, '2018_05_18_180811_create_items_table', 33),
(90, '2018_05_18_182509_create_color_item_table', 34),
(91, '2018_05_18_194317_add_color_id_column_in_item_images_table', 35),
(92, '2018_05_19_150357_add_activation_at_column_in_items_table', 36),
(93, '2018_05_21_085041_add_meta_vendor_id_in_items_column', 37),
(94, '2018_05_21_104611_change_descrtiption_to_nullable_in_items_table', 38),
(95, '2018_05_21_181848_add_sort_column_in_item_images_table', 39),
(96, '2018_05_22_183025_add_image_path_column_in_master_colors_table', 40),
(97, '2018_05_24_155751_create_meta_buyers_table', 41),
(99, '2018_05_25_052549_create_cart_items_table', 42),
(100, '2018_05_25_150654_add_active_column_in_meta_vendors_table', 43),
(101, '2018_05_26_115243_add_location_columns_in_meta_vendors_table', 44),
(103, '2018_05_28_135447_create_vendor_images_table', 45),
(105, '2018_05_28_182550_create_orders_table', 46),
(106, '2018_05_28_200922_add_vendor_meta_id_column_in_cart_items_table', 47),
(109, '2018_05_29_085525_create_order_items_table', 48),
(111, '2018_05_29_095042_add_order_number_column_in_orders_table', 49),
(112, '2018_05_29_164751_add_per_unit_price_column_in_order_items_table', 50),
(113, '2018_05_30_083752_create_notifications_table', 51),
(114, '2018_05_30_181718_add_verify_active_column_in_meta_buyers_table', 52),
(115, '2018_05_30_183944_add_block_column_in_meta_buyers_table', 53),
(116, '2018_05_31_055248_add_shipping_location_column_in_meta_buyers_table', 54),
(118, '2018_05_31_060140_add_store_no_column_in_meta_buyers_table', 55),
(119, '2018_05_31_192012_create_couriers_table', 56),
(120, '2018_06_01_083044_create_admin_ship_methods_table', 57),
(121, '2018_06_01_094139_create_shipping_methods_table', 58),
(122, '2018_06_01_191253_add_shipping_method_id_in_orders_table', 59),
(123, '2018_06_04_090630_add_show_main_slider_column_in_meta_vendors_table', 60),
(124, '2018_06_05_090127_create_visitors_table', 61),
(125, '2018_06_07_204324_create_category_banners_table', 62),
(126, '2018_06_08_101221_add_main_slider_column_in_items_table', 63),
(127, '2018_06_08_180010_add_soft_delete_in_orders_table', 64),
(128, '2018_06_09_153920_add_shipping_location_column_in_orders_table', 65),
(129, '2018_06_09_155854_add_shipping_country_id_column_in_orders_table', 66),
(130, '2018_06_10_085852_add_code_column_in_colors_table', 67),
(131, '2018_06_10_111156_add_tracking_number_column_in_orders_table', 68),
(133, '2018_06_11_090147_add_user_id_column_in_meta_buyers_table', 69),
(134, '2018_06_11_154425_change_card_number_length_in_orders_table', 70),
(135, '2018_06_11_190820_add_new_top_slider_column_in_items_table', 71),
(136, '2018_06_12_151328_create_wish_list_items_table', 72),
(138, '2018_06_12_181955_add_list_image_path_in_item_images_table', 73),
(139, '2018_06_12_190655_add_thumbs_image_path_in_item_images_table', 74),
(140, '2018_06_13_090427_add_style_no_column_in_order_items_table', 75),
(141, '2018_06_13_093013_add_company_name_column_in_orders_table', 76),
(142, '2018_06_13_094203_add_name_column_in_orders_table', 77),
(143, '2018_06_13_094740_add_shipping_column_in_orders_table', 78),
(144, '2018_06_13_105024_add_notes_column_in_orders_table', 79),
(145, '2018_06_13_160933_add_image_path_column_in_colors_table', 80),
(146, '2018_06_13_180012_create_slider_items_table', 81),
(147, '2018_06_22_090108_remove_sub_category_from_body_sizes_table', 82),
(148, '2018_06_22_090425_add_parent_category_id_in_body_sizes_table', 83),
(149, '2018_06_22_101056_change_sub_category_id_to_parent_category_id_in_patterns_table', 84),
(150, '2018_06_22_102143_change_sub_category_id_to_parent_category_id_in_styles_table', 85),
(152, '2018_06_22_124629_create_buyer_shipping_addresses_table', 86),
(153, '2018_06_22_130248_remove_shipping_address_from_meta_buyers_table', 87),
(154, '2018_06_22_141254_rename_state_to_state_text_in_buyer_shipping_address_table', 88),
(155, '2018_06_22_175526_add_shipping_address_id_in_orders_table', 89),
(156, '2018_06_26_095141_add_user_id_column_in_visitors_table', 90),
(157, '2018_06_26_103908_create_block_users_table', 91),
(158, '2018_06_28_164609_add_fabric_column_in_items_table', 92);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tracking_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address_id` int(11) DEFAULT NULL,
  `shipping_method_id` int(11) DEFAULT NULL,
  `shipping_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_state_id` int(11) DEFAULT NULL,
  `shipping_zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_country_id` int(11) DEFAULT NULL,
  `shipping_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_state_id` int(11) DEFAULT NULL,
  `billing_zip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `billing_country_id` int(11) DEFAULT NULL,
  `billing_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_number` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_full_name` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_expire` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_cvc` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subtotal` double(8,2) DEFAULT NULL,
  `discount` double(8,2) DEFAULT NULL,
  `shipping_cost` double(8,2) DEFAULT NULL,
  `total` double(8,2) DEFAULT NULL,
  `note` varchar(1500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `status`, `user_id`, `order_number`, `tracking_number`, `name`, `email`, `company_name`, `shipping`, `shipping_address_id`, `shipping_method_id`, `shipping_location`, `shipping_address`, `shipping_city`, `shipping_state`, `shipping_state_text`, `shipping_state_id`, `shipping_zip`, `shipping_country`, `shipping_country_id`, `shipping_phone`, `billing_location`, `billing_address`, `billing_city`, `billing_state`, `billing_state_text`, `billing_state_id`, `billing_zip`, `billing_country`, `billing_country_id`, `billing_phone`, `card_number`, `card_full_name`, `card_expire`, `card_cvc`, `subtotal`, `discount`, `shipping_cost`, `total`, `note`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 14, '4AA29IFHEF93G', NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'US', 'Raj', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, 762.00, 0.00, 0.00, 762.00, NULL, '2018-07-13 08:57:35', '2018-07-13 08:57:35', NULL),
(2, 1, 14, 'OIS1TL98H7EX1', NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', NULL, 17, NULL, 'US', 'tttete', 'wer', 'Arizona', NULL, 4, NULL, 'United States', 1, '234', 'US', 'Raj', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', NULL, NULL, NULL, NULL, 762.00, 0.00, 0.00, 762.00, NULL, '2018-07-13 08:59:57', '2018-07-13 09:16:55', NULL),
(3, 2, 14, 'NRM4OBXH5B5HW', NULL, 'First Customer', 'shantotrs@gmail.com', 'Buyer LTD', 'UPS 23', 6, 2, 'US', 'asdf', 'dfadf', 'Alaska', NULL, 1, NULL, 'United States', 1, '3434', 'US', 'Raj', 'gsdfgfga', 'District of Columbia', NULL, 10, '5656', 'United States', 1, '4576767', 'eyJpdiI6Im5VbjZWdW50TW5oMUVnZ0RLUG5EWlE9PSIsInZhbHVlIjoiN0V2aHhsSmlIaW5XOEFUeVgrOXcxSXlYMGd0M1I2NFNET2h2WUxXV1Z3MD0iLCJtYWMiOiJjNzRmMWNmMzM5NTY3MWEyYTFiNTJjYzQ2YTU2OTEwMmRhNmIxODJmNDc1NzAzMzE1ZTg3NjMzMjExMjZkNDdmIn0=', 'eyJpdiI6ImtmemNmZG5PSTB6UHpycGMwU3pSNFE9PSIsInZhbHVlIjoieDRwZDR5bWE3bXp3ZTR6V2l6KzJEZz09IiwibWFjIjoiZTZhNzBiOGQ4MzdkMTBhNDZhY2ZkMDllMTUwODFlYzU2MGNhMDlkYTBlMDJkZGQwNzRiOThiYzlmZWQ2OTQ0NiJ9', 'eyJpdiI6IkcwVjFlNFVtQk5xSVdzbkNrRXg0eWc9PSIsInZhbHVlIjoiT1V1elRibWI2WkdZMDY0aWxacEJkQT09IiwibWFjIjoiNWUyMGNjYzQ3ZmE2NTRiMmNkMDU3YTg2YTg4MDU2NzljYmQzOWI0ZGUyZThmNjQ0NzI5MzFmNmVmMDQ5MDJlOCJ9', 'eyJpdiI6IjJtaWE5dmFqcmlzT2NnRlJjUFUxMVE9PSIsInZhbHVlIjoiMTQxd2xuSXBNMnI5azJ1dXpDUXV4Zz09IiwibWFjIjoiM2Y0MWU3OTlhMDY1MDYyYTNlY2IzYTE0NDU1NjI2MGFlNDlhZmZhNWYyYjY4MjVlZTRiNTFkZTEyOGI2MGNjMyJ9', 762.00, 0.00, 0.00, 762.00, 'new note', '2018-07-13 09:26:05', '2018-07-13 09:54:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

DROP TABLE IF EXISTS `order_items`;
CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `style_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_per_pack` int(11) NOT NULL,
  `pack` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `total_qty` int(11) NOT NULL,
  `per_unit_price` double(8,2) NOT NULL,
  `amount` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_id`, `item_id`, `style_no`, `color`, `size`, `item_per_pack`, `pack`, `qty`, `total_qty`, `per_unit_price`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, 5, 'N01', 'SILVER', 'S-M-L', 6, '2-2-2', 5, 30, 12.00, 360.00, '2018-07-13 08:57:35', '2018-07-13 08:57:35'),
(2, 1, 7, 'N734', 'GREY', 'S-M-L', 6, '2-2-2', 1, 6, 67.00, 402.00, '2018-07-13 08:57:35', '2018-07-13 08:57:35'),
(3, 2, 5, 'N01', 'SILVER', 'S-M-L', 6, '2-2-2', 5, 30, 12.00, 360.00, '2018-07-13 08:59:57', '2018-07-13 08:59:57'),
(4, 2, 7, 'N734', 'GREY', 'S-M-L', 6, '2-2-2', 1, 6, 67.00, 402.00, '2018-07-13 08:59:57', '2018-07-13 08:59:57'),
(5, 3, 5, 'N01', 'SILVER', 'S-M-L', 6, '2-2-2', 5, 30, 12.00, 360.00, '2018-07-13 09:26:05', '2018-07-13 09:26:05'),
(6, 3, 7, 'N734', 'GREY', 'S-M-L', 6, '2-2-2', 1, 6, 67.00, 402.00, '2018-07-13 09:26:05', '2018-07-13 09:26:05');

-- --------------------------------------------------------

--
-- Table structure for table `packs`
--

DROP TABLE IF EXISTS `packs`;
CREATE TABLE IF NOT EXISTS `packs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `pack1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pack2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack9` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pack10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `packs`
--

INSERT INTO `packs` (`id`, `name`, `status`, `default`, `pack1`, `pack2`, `pack3`, `pack4`, `pack5`, `pack6`, `pack7`, `pack8`, `pack9`, `pack10`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'S-M-L', 1, 0, '2', '2', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-11 05:16:28', '2018-06-29 10:20:48', NULL),
(2, '3-3', 1, 0, '3', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-11 05:30:54', '2018-06-29 10:20:48', NULL),
(3, 'ONE SIZE', 1, 0, '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-16 11:39:34', '2018-06-29 10:20:48', NULL),
(4, '2/2/2', 1, 0, '2', '2', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-23 15:21:18', '2018-06-29 10:20:48', NULL),
(5, '3/3', 1, 0, '3', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-23 15:21:28', '2018-06-29 10:20:48', NULL),
(6, 'ONE SIZE', 1, 0, '6', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-23 15:21:38', '2018-06-29 10:20:48', NULL),
(7, 'dsfd', 1, 1, '1', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-26 08:59:37', '2018-06-29 10:20:48', NULL),
(8, '123', 1, 0, '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-26 10:34:42', '2018-05-26 10:42:56', '2018-05-26 10:42:56'),
(9, 'a-u', 0, 0, '2', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-29 10:19:56', '2018-06-29 10:20:52', '2018-06-29 10:20:52'),
(10, '2-3-e-5', 1, 0, '34', '54', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-06-30 09:20:07', '2018-06-30 09:27:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patterns`
--

DROP TABLE IF EXISTS `patterns`;
CREATE TABLE IF NOT EXISTS `patterns` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patterns`
--

INSERT INTO `patterns` (`id`, `name`, `parent_category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', 1, '2018-05-17 06:44:46', '2018-05-17 06:44:52', '2018-05-17 06:44:52'),
(2, 'Burnout', 1, '2018-05-17 06:44:57', '2018-05-22 14:19:31', NULL),
(3, 'Check', 1, '2018-05-22 14:19:38', '2018-05-22 14:19:38', NULL),
(4, 'Color block', 1, '2018-05-22 14:19:46', '2018-05-22 14:19:46', NULL),
(5, 'Crochet', 1, '2018-05-22 14:19:55', '2018-05-22 14:19:55', NULL),
(6, 'Digital Print', 1, '2018-05-22 14:20:02', '2018-05-22 14:20:02', NULL),
(7, 'Embroidered', 1, '2018-05-22 14:20:11', '2018-05-22 14:20:11', NULL),
(8, 'Glitter/Sequins', 1, '2018-05-22 14:20:17', '2018-05-22 14:20:17', NULL),
(9, 'Lace', 1, '2018-05-22 14:20:27', '2018-05-22 14:20:27', NULL),
(10, 'Lettering', 1, '2018-05-22 14:20:33', '2018-05-22 14:20:33', NULL),
(11, 'Metallic', 1, '2018-05-22 14:20:41', '2018-05-22 14:20:41', NULL),
(12, 'Multi-Color', 1, '2018-05-22 14:20:47', '2018-05-22 14:20:47', NULL),
(13, 'Multicolor', 1, '2018-05-22 14:20:54', '2018-05-22 14:20:54', NULL),
(14, 'Ombre', 1, '2018-05-22 14:21:02', '2018-05-22 14:21:02', NULL),
(15, 'Plaid', 1, '2018-05-22 14:21:10', '2018-05-22 14:21:10', NULL),
(16, 'Polka Dot', 1, '2018-05-22 14:21:17', '2018-05-22 14:21:17', NULL),
(17, 'Print Screen', 1, '2018-05-22 14:21:24', '2018-05-22 14:21:24', NULL),
(18, 'Print Sublimation', 1, '2018-05-22 14:21:31', '2018-05-22 14:21:31', NULL),
(19, 'Print-Animal-Floral-Skull-Butterfly', 1, '2018-05-22 14:21:37', '2018-05-22 14:21:37', NULL),
(20, 'Rhinestones', 1, '2018-05-22 14:21:44', '2018-05-22 14:21:44', NULL),
(21, 'Ribbon/Bows', 1, '2018-05-22 14:21:51', '2018-05-22 14:21:51', NULL),
(22, 'Solid', 1, '2018-05-22 14:21:59', '2018-05-22 14:21:59', NULL),
(23, 'Striped', 1, '2018-05-22 14:22:07', '2018-05-22 14:22:07', NULL),
(24, 'Striped Sublimation', 1, '2018-05-22 14:22:15', '2018-05-22 14:22:15', NULL),
(27, 'tt', 5, '2018-06-22 04:18:28', '2018-06-22 04:18:41', '2018-06-22 04:18:41');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_methods`
--

DROP TABLE IF EXISTS `shipping_methods`;
CREATE TABLE IF NOT EXISTS `shipping_methods` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `courier_id` int(11) NOT NULL,
  `ship_method_id` int(11) NOT NULL,
  `list_order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `shipping_methods`
--

INSERT INTO `shipping_methods` (`id`, `status`, `default`, `courier_id`, `ship_method_id`, `list_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, 1, 1, '2018-06-01 04:41:24', '2018-06-25 10:20:16', NULL),
(2, 1, 0, 1, 3, 2, '2018-06-01 04:41:42', '2018-06-25 10:20:16', NULL),
(3, 1, 0, 3, 5, 1, '2018-06-04 09:22:27', '2018-07-02 09:20:12', '2018-07-02 09:20:12'),
(4, 1, 0, 1, 1, 3, '2018-07-02 11:08:02', '2018-07-02 11:08:12', '2018-07-02 11:08:12');

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

DROP TABLE IF EXISTS `sizes`;
CREATE TABLE IF NOT EXISTS `sizes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `size1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size6` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size7` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size8` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size9` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `name`, `status`, `default`, `user_id`, `size1`, `size2`, `size3`, `size4`, `size5`, `size6`, `size7`, `size8`, `size9`, `size10`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Test Size', 1, 0, 1, 'S', 'M', 'L', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-09 06:42:40', '2018-05-11 08:16:24', NULL),
(2, 'Test Size 2', 0, 1, 1, 'S', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-05-10 03:25:13', '2018-05-11 08:16:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `slider_items`
--

DROP TABLE IF EXISTS `slider_items`;
CREATE TABLE IF NOT EXISTS `slider_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `sort` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slider_items`
--

INSERT INTO `slider_items` (`id`, `item_id`, `sort`, `type`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 1, '2018-07-02 07:34:35', '2018-07-02 07:34:40'),
(2, 4, 2, 1, '2018-07-02 07:34:37', '2018-07-02 07:34:40'),
(3, 2, 1, 4, '2018-07-02 07:34:46', '2018-07-02 07:34:46');

-- --------------------------------------------------------

--
-- Table structure for table `states`
--

DROP TABLE IF EXISTS `states`;
CREATE TABLE IF NOT EXISTS `states` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `country_id`, `name`, `code`) VALUES
(1, 1, 'Alaska', 'AK'),
(2, 1, 'Alabama', 'AL'),
(3, 1, 'American Samoa', 'AS'),
(4, 1, 'Arizona', 'AZ'),
(5, 1, 'Arkansas', 'AR'),
(6, 1, 'California', 'CA'),
(7, 1, 'Colorado', 'CO'),
(8, 1, 'Connecticut', 'CT'),
(9, 1, 'Delaware', 'DE'),
(10, 1, 'District of Columbia', 'DC'),
(11, 1, 'Federated States of Micronesia', 'FM'),
(12, 1, 'Florida', 'FL'),
(13, 1, 'Georgia', 'GA'),
(14, 1, 'Guam', 'GU'),
(15, 1, 'Hawaii', 'HI'),
(16, 1, 'Idaho', 'ID'),
(17, 1, 'Illinois', 'IL'),
(18, 1, 'Indiana', 'IN'),
(19, 1, 'Iowa', 'IA'),
(20, 1, 'Kansas', 'KS'),
(21, 1, 'Kentucky', 'KY'),
(22, 1, 'Louisiana', 'LA'),
(23, 1, 'Maine', 'ME'),
(24, 1, 'Marshall Islands', 'MH'),
(25, 1, 'Maryland', 'MD'),
(26, 1, 'Massachusetts', 'MA'),
(27, 1, 'Michigan', 'MI'),
(28, 1, 'Minnesota', 'MN'),
(29, 1, 'Mississippi', 'MS'),
(30, 1, 'Missouri', 'MO'),
(31, 1, 'Montana', 'MT'),
(32, 1, 'Nebraska', 'NE'),
(33, 1, 'Nevada', 'NV'),
(34, 1, 'New Hampshire', 'NH'),
(35, 1, 'New Jersey', 'NJ'),
(36, 1, 'New Mexico', 'NM'),
(37, 1, 'New York', 'NY'),
(38, 1, 'North Carolina', 'NC'),
(39, 1, 'North Dakota', 'ND'),
(40, 1, 'Northern Mariana Islands', 'MP'),
(41, 1, 'Ohio', 'OH'),
(42, 1, 'Oklahoma', 'OK'),
(43, 1, 'Oregon', 'OR'),
(44, 1, 'Palau', 'PW'),
(45, 1, 'Pennsylvania', 'PA'),
(46, 1, 'Puerto Rico', 'PR'),
(47, 1, 'Rhode Island', 'RI'),
(48, 1, 'South Carolina', 'SC'),
(49, 1, 'South Dakota', 'SD'),
(50, 1, 'Tennessee', 'TN'),
(51, 1, 'Texas', 'TX'),
(52, 1, 'Utah', 'UT'),
(53, 1, 'Vermont', 'VT'),
(54, 1, 'Virgin Islands', 'VI'),
(55, 1, 'Virginia', 'VA'),
(56, 1, 'Washington', 'WA'),
(57, 1, 'West Virginia', 'WV'),
(58, 1, 'Wisconsin', 'WI'),
(59, 1, 'Wyoming', 'WY'),
(60, 1, 'Armed Forces Africa', 'AE'),
(61, 1, 'Armed Forces Americas (except Canada)', 'AA'),
(62, 1, 'Armed Forces Canada', 'AE'),
(63, 1, 'Armed Forces Europe', 'AE'),
(64, 1, 'Armed Forces Middle East', 'AE'),
(65, 1, 'Armed Forces Pacific', 'AP'),
(66, 2, 'Alberta', 'AB'),
(67, 2, 'British Columbia', 'BC'),
(68, 2, 'Manitoba', 'MB'),
(69, 2, 'New Brunswick', 'NB'),
(70, 2, 'Newfoundland and Labrador', 'NL'),
(71, 2, 'Northwest Territories', 'NT'),
(72, 2, 'Nova Scotia', 'NS'),
(73, 2, 'Nunavut', 'NU'),
(74, 2, 'Ontario', 'ON'),
(75, 2, 'Prince Edward Island', 'PE'),
(76, 2, 'Quebec', 'QC'),
(77, 2, 'Saskatchewan', 'SK'),
(78, 2, 'Yukon', 'YT');

-- --------------------------------------------------------

--
-- Table structure for table `styles`
--

DROP TABLE IF EXISTS `styles`;
CREATE TABLE IF NOT EXISTS `styles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_category_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `styles`
--

INSERT INTO `styles` (`id`, `name`, `parent_category_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'A-line', 1, '2018-05-17 07:34:48', '2018-05-22 14:24:20', NULL),
(2, 'asdfa', 1, '2018-05-17 07:34:57', '2018-05-17 07:35:00', '2018-05-17 07:35:00'),
(3, 'Asymmetrical', 1, '2018-05-22 14:24:27', '2018-05-22 14:24:27', NULL),
(4, 'Babydoll', 1, '2018-05-22 14:24:33', '2018-05-22 14:24:33', NULL),
(5, 'Beaded', 1, '2018-05-22 14:24:40', '2018-05-22 14:24:40', NULL),
(6, 'Belted', 1, '2018-05-22 14:24:48', '2018-05-22 14:24:48', NULL),
(7, 'Blouse', 1, '2018-05-22 14:24:56', '2018-05-22 14:24:56', NULL),
(8, 'fd', 3, '2018-06-22 04:36:09', '2018-06-22 04:36:20', '2018-06-22 04:36:20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `buyer_meta_id` int(11) DEFAULT NULL,
  `vendor_meta_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `user_id`, `password`, `role`, `active`, `buyer_meta_id`, `vendor_meta_id`, `remember_token`, `last_login`, `created_at`, `updated_at`) VALUES
(1, 'Mr.', 'ss', 'vendor@gmail.com', 'vendor', '$2y$10$Uumgr3En9yFT5fEUBw4OFeulreuynvKWNP2SnuYI2ic2vU8WpUFaG', 2, 1, NULL, 1, 'TVkgvzajqsZWFEMojdQWfc2bn3qH2qHJcTntjxi7kqLyoPqcqtpgOA2Udgmk', '2018-07-13 09:18:56', '2018-05-08 09:03:20', '2018-07-13 09:18:56'),
(2, 'Super', 'Admin', 'admin@gmail.com', NULL, '$2y$10$Q1pYXTCr.5Dxasr4byZR5uTxs3x8aFXDpPL7CX/r.6mHC1muwBw9m', 1, 1, NULL, NULL, 'kIjugAmxEaazEdIDfJg2zRKJ5ioAl6fcZMOkxKgc7Ukj9OQZ3faLHqc4AiP0', NULL, '2018-05-10 05:41:54', '2018-05-10 05:41:54'),
(3, 'dd3ye', 'dder', NULL, '4352', '$2y$10$XDxkIkQ8yMarz5yjGxqIXOWzBx540aGXl10BOsViq9reiQw0Bq7iO', 4, 1, NULL, NULL, NULL, NULL, '2018-05-15 08:10:43', '2018-06-28 13:01:12'),
(11, 'asdf', 'cvcc', NULL, 'sdf', '$2y$10$K2uvm3GOUItDXNBXoQxYNugJA2XpRtSKilZ.reKSia8BkYAuFNW92', 4, 1, NULL, NULL, NULL, NULL, '2018-05-15 11:15:15', '2018-06-25 08:30:46'),
(14, 'First', 'Customer', 'shantotrs@gmail.com', NULL, '$2y$10$JXoRQJYy/cufBYi44Avkt.BOBWUixcdqQUa/JkbgGrlsw4wR/eScG', 3, 1, 3, NULL, 'sL6oLMFxLqwySpBrwTadLerW6x4sDmADF6BhIr7zDWeWLMP6bkjRSKw0dMDl', '2018-07-13 07:09:23', '2018-05-24 10:22:35', '2018-07-13 07:09:23'),
(20, 'Employee', 'Test', NULL, 'employee', '$2y$10$1oHUJ89WqwtBEeJJ43i.T.spUPim.uQezevTCVsvXheFFyERiKjMi', 2, 1, NULL, 1, NULL, NULL, '2018-07-02 09:05:10', '2018-07-02 09:05:10'),
(19, 'New', 'buyer', 'newbuyer@gmail.com', NULL, '$2y$10$2YyXpCQy32JDLcWWEzV1NeqZjGGgnwHTwx24teL53ipi1N13uZlvS', 3, 1, 4, NULL, NULL, NULL, '2018-06-22 07:18:27', '2018-06-22 07:18:27'),
(22, 'Fame', 'Testt', 'fame@gmail.com', NULL, '$2y$10$XmWdEcRuMJ1fUlnBZyUN0OM1LcC0DBUR9zchU6n692Xicr4xCFDna', 3, 1, 7, NULL, NULL, NULL, '2018-07-12 07:57:38', '2018-07-12 07:57:38');

-- --------------------------------------------------------

--
-- Table structure for table `user_permission`
--

DROP TABLE IF EXISTS `user_permission`;
CREATE TABLE IF NOT EXISTS `user_permission` (
  `user_id` int(11) NOT NULL,
  `permission` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_permission`
--

INSERT INTO `user_permission` (`user_id`, `permission`) VALUES
(9, 3),
(9, 4),
(10, 2),
(10, 6),
(11, 10),
(11, 4),
(11, 3),
(20, 3),
(20, 4);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_images`
--

DROP TABLE IF EXISTS `vendor_images`;
CREATE TABLE IF NOT EXISTS `vendor_images` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `image_path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendor_images`
--

INSERT INTO `vendor_images` (`id`, `type`, `status`, `image_path`, `sort`, `created_at`, `updated_at`) VALUES
(2, 1, 1, '/images/banner/5d7947f0-7df9-11e8-b00c-1fc92e30ed17.jpg', NULL, '2018-07-02 07:11:00', '2018-07-02 07:11:19'),
(3, 4, 1, '/images/banner/733d1f00-7df9-11e8-80a2-af0b4d34e495.jpg', NULL, '2018-07-02 07:11:36', '2018-07-02 07:11:39'),
(11, 7, 1, '/images/banner/28f3b960-81ff-11e8-8038-35b0903c882b.jpg', 2, '2018-07-07 10:02:33', '2018-07-07 10:02:51'),
(12, 7, 1, '/images/banner/2ba8a670-81ff-11e8-98c1-ffa0ad352487.jpg', 1, '2018-07-07 10:02:38', '2018-07-07 10:02:51'),
(13, 8, 1, '/images/banner/cd322e20-83a2-11e8-a6e3-bb1ef1eb06af.jpg', 3, '2018-07-09 12:06:28', '2018-07-09 12:09:36'),
(14, 8, 1, '/images/banner/d03fbc20-83a2-11e8-a3de-e31afaa0ced3.jpg', 1, '2018-07-09 12:06:33', '2018-07-09 12:09:36'),
(15, 8, 1, '/images/banner/d2c10540-83a2-11e8-a969-9d7108a652e3.jpg', 2, '2018-07-09 12:06:37', '2018-07-09 12:09:36'),
(16, 2, 1, '/images/banner/ddd0ef20-83a3-11e8-8c96-a56cd4e2b40b.jpg', NULL, '2018-07-09 12:14:05', '2018-07-09 12:14:07'),
(17, 2, 0, '/images/banner/23d2f410-83a4-11e8-9800-139683fece47.jpg', NULL, '2018-07-09 12:16:03', '2018-07-09 12:16:03');

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

DROP TABLE IF EXISTS `visitors`;
CREATE TABLE IF NOT EXISTS `visitors` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=94 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`id`, `user_id`, `url`, `ip`, `created_at`, `updated_at`) VALUES
(1, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '::1', '2018-06-05 03:09:06', '2018-06-05 03:09:06'),
(2, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '::1', '2018-06-05 03:09:50', '2018-06-05 03:09:50'),
(3, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '192:12:1', '2018-06-05 03:09:57', '2018-06-05 03:09:57'),
(4, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-08 10:31:36', '2018-06-08 10:31:36'),
(5, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-08 11:35:39', '2018-06-08 11:35:39'),
(6, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-08 11:35:42', '2018-06-08 11:35:42'),
(7, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 02:01:51', '2018-06-09 02:01:51'),
(8, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:17:25', '2018-06-09 12:17:25'),
(9, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:17:37', '2018-06-09 12:17:37'),
(10, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:19:01', '2018-06-09 12:19:01'),
(11, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:20:01', '2018-06-09 12:20:01'),
(12, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:33:35', '2018-06-09 12:33:35'),
(13, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:33:56', '2018-06-09 12:33:56'),
(14, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:54:55', '2018-06-09 12:54:55'),
(15, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:56:28', '2018-06-09 12:56:28'),
(16, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:56:43', '2018-06-09 12:56:43'),
(17, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-09 12:58:40', '2018-06-09 12:58:40'),
(18, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 02:04:04', '2018-06-10 02:04:04'),
(19, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 04:20:33', '2018-06-10 04:20:33'),
(20, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 04:22:04', '2018-06-10 04:22:04'),
(21, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 04:22:18', '2018-06-10 04:22:18'),
(22, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 04:22:58', '2018-06-10 04:22:58'),
(23, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 04:22:59', '2018-06-10 04:22:59'),
(24, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 04:23:08', '2018-06-10 04:23:08'),
(25, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 04:23:18', '2018-06-10 04:23:18'),
(26, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-10 04:23:39', '2018-06-10 04:23:39'),
(27, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-11 02:54:35', '2018-06-11 02:54:35'),
(28, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-11 02:54:38', '2018-06-11 02:54:38'),
(29, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-11 02:54:41', '2018-06-11 02:54:41'),
(30, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-11 02:54:44', '2018-06-11 02:54:44'),
(31, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-11 02:54:47', '2018-06-11 02:54:47'),
(32, NULL, 'http://localhost/wholesalepeople/public/vendor/home/7', '3', '2018-06-11 03:10:50', '2018-06-11 03:10:50'),
(33, NULL, 'http://localhost/wholesalepeople/public/vendor/home/7', '3', '2018-06-11 03:11:38', '2018-06-11 03:11:38'),
(34, NULL, 'http://localhost/wholesalepeople/public/vendor/home/7', '3', '2018-06-11 03:12:00', '2018-06-11 03:12:00'),
(35, NULL, 'http://localhost/wholesalepeople/public/vendor/home/7', '3', '2018-06-11 03:13:15', '2018-06-11 03:13:15'),
(36, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-11 11:22:47', '2018-06-11 11:22:47'),
(37, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 02:38:32', '2018-06-12 02:38:32'),
(38, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 10:52:16', '2018-06-12 10:52:16'),
(39, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:06:34', '2018-06-12 11:06:34'),
(40, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:10:20', '2018-06-12 11:10:20'),
(41, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:10:42', '2018-06-12 11:10:42'),
(42, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:10:57', '2018-06-12 11:10:57'),
(43, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:11:04', '2018-06-12 11:11:04'),
(44, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:11:34', '2018-06-12 11:11:34'),
(45, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:11:35', '2018-06-12 11:11:35'),
(46, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:12:01', '2018-06-12 11:12:01'),
(47, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:12:03', '2018-06-12 11:12:03'),
(48, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:12:06', '2018-06-12 11:12:06'),
(49, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-12 11:17:46', '2018-06-12 11:17:46'),
(50, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-13 11:20:58', '2018-06-13 11:20:58'),
(51, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-13 11:21:21', '2018-06-13 11:21:21'),
(52, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-13 11:21:43', '2018-06-13 11:21:43'),
(53, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-13 11:21:50', '2018-06-13 11:21:50'),
(54, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-13 11:21:52', '2018-06-13 11:21:52'),
(55, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-13 13:51:00', '2018-06-13 13:51:00'),
(56, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-13 14:18:16', '2018-06-13 14:18:16'),
(57, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-13 14:18:24', '2018-06-13 14:18:24'),
(58, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-13 14:18:27', '2018-06-13 14:18:27'),
(59, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-13 14:18:30', '2018-06-13 14:18:30'),
(60, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 05:20:59', '2018-06-23 05:20:59'),
(61, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:45:10', '2018-06-23 07:45:10'),
(62, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:45:16', '2018-06-23 07:45:16'),
(63, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:46:50', '2018-06-23 07:46:50'),
(64, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:47:42', '2018-06-23 07:47:42'),
(65, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:47:43', '2018-06-23 07:47:43'),
(66, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:49:18', '2018-06-23 07:49:18'),
(67, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:49:49', '2018-06-23 07:49:49'),
(68, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:51:20', '2018-06-23 07:51:20'),
(69, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 07:53:40', '2018-06-23 07:53:40'),
(70, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 11:11:30', '2018-06-23 11:11:30'),
(71, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 11:12:38', '2018-06-23 11:12:38'),
(72, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-23 11:12:47', '2018-06-23 11:12:47'),
(73, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-23 11:40:49', '2018-06-23 11:40:49'),
(74, 14, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-26 04:18:28', '2018-06-26 04:18:28'),
(75, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-27 03:02:56', '2018-06-27 03:02:56'),
(76, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-27 04:45:10', '2018-06-27 04:45:10'),
(77, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-27 11:29:16', '2018-06-27 11:29:16'),
(78, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-27 11:29:26', '2018-06-27 11:29:26'),
(79, 14, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-27 12:08:07', '2018-06-27 12:08:07'),
(80, 14, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-27 12:08:10', '2018-06-27 12:08:10'),
(81, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 03:38:21', '2018-06-28 03:38:21'),
(82, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 03:38:26', '2018-06-28 03:38:26'),
(83, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-28 03:41:02', '2018-06-28 03:41:02'),
(84, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-28 03:41:09', '2018-06-28 03:41:09'),
(85, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-28 03:41:10', '2018-06-28 03:41:10'),
(86, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 03:41:31', '2018-06-28 03:41:31'),
(87, NULL, 'http://localhost/wholesalepeople/public/vendor/home/2', '3', '2018-06-28 03:41:35', '2018-06-28 03:41:35'),
(88, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 03:41:39', '2018-06-28 03:41:39'),
(89, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 03:41:58', '2018-06-28 03:41:58'),
(90, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 03:42:01', '2018-06-28 03:42:01'),
(91, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 04:09:31', '2018-06-28 04:09:31'),
(92, 14, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 06:42:19', '2018-06-28 06:42:19'),
(93, NULL, 'http://localhost/wholesalepeople/public/vendor/home/1', '3', '2018-06-28 12:35:06', '2018-06-28 12:35:06');

-- --------------------------------------------------------

--
-- Table structure for table `wish_list_items`
--

DROP TABLE IF EXISTS `wish_list_items`;
CREATE TABLE IF NOT EXISTS `wish_list_items` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wish_list_items`
--

INSERT INTO `wish_list_items` (`id`, `user_id`, `item_id`, `created_at`, `updated_at`) VALUES
(32, 14, 8, '2018-07-13 08:34:42', '2018-07-13 08:34:42');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
