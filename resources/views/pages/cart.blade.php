@extends('layouts.app')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <style>
        .cart_total ul li span {
            margin-bottom: 6px!important;
        }
    </style>
@stop

@section('content')
    <section class="shipping_cart_area">
        <div class="cart_inner_content clearfix">
            <h2>SHOPPING CART</h2>
            <?php $subTotal = 0; ?>
            <div class="cart_table table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="product-thumbnail"><b>Image</b></th>
                        <th><b>Style No.</b></th>
                        <th class="text-center"><b>Color</b></th>
                        <th class="text-center" colspan="10"><b>Size</b></th>
                        <th class="text-center"><b>Pack</b></th>
                        <th class="text-center"><b>Total Qty</b></th>
                        <th class="text-center"><b>Unit Price</b></th>
                        <th class="text-center"><b>Amount</b></th>
                        <th class="text-center"><b>Delete</b></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cartItems as $item_index => $items)
                        <tr>
                            <td rowspan="{{ sizeof($items)+1  }}">
                                @if (sizeof($items[0]->item->images) > 0)
                                    <img src="{{ asset($items[0]->item->images[0]->list_image_path) }}" alt="Product" width="50px">
                                @else
                                    <img src="{{ asset('images/no-image.png') }}" alt="Product" width="50px">
                                @endif
                            </td>
                            <td rowspan="{{ sizeof($items)+1 }}">
                                <a href="{{ route('item_details_page', ['item' => $items[0]->item->id, 'name' => changeSpecialChar($items[0]->item->name)]) }}">{{ $items[0]->item->style_no }}</a>
                            </td>

                            <td class="text-center text-lg text-medium">&nbsp;</td>

                            <?php
                            $sizes = explode("-", $items[0]->item->pack->name);
                            $itemInPack = 0;

                            for($i=1; $i <= sizeof($sizes); $i++) {
                                $var = 'pack'.$i;

                                if ($items[0]->item->pack->$var != null)
                                    $itemInPack += (int) $items[0]->item->pack->$var;
                            }
                            ?>

                            @foreach($sizes as $size)
                                <th class="text-center" colspan="{{ $loop->last ? 10-sizeof($sizes) +1 : '' }}"><b>{{ $size }}</b></th>
                            @endforeach

                            <td>
                                &nbsp;
                            </td>

                            <td>
                                &nbsp;
                            </td>

                            <td>
                                &nbsp;
                            </td>

                            <td>
                                &nbsp;
                            </td>

                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        @foreach($items as $item)
                            <tr>
                                <td class="text-center">
                                    {{ $item->color->name }}
                                </td>

                                @for($i=1; $i <= sizeof($sizes); $i++)
                                    <?php $p = 'pack'.$i; ?>
                                    <td class="text-center" colspan="{{ $i == sizeof($sizes) ? 10-$i +1 : '' }}">{{ ($items[0]->item->pack->$p == null) ? '0' : $items[0]->item->pack->$p }}</td>
                                @endfor

                                <td class="text-center">
                                    <input class="input_qty form-control"
                                           value="{{ $item->quantity }}"
                                           data-per-pack="{{ $itemInPack }}"
                                           data-price="{{ $item->item->price }}"
                                           data-id="{{ $item->id }}" style="height: auto; width: 50px; display: inline">
                                </td>

                                <td class="text-center">
                                    <span class="total_qty">{{ $itemInPack * $item->quantity }}</span>
                                </td>

                                <td class="text-center">
                                    ${{ sprintf('%0.2f', $item->item->price) }}
                                </td>

                                <td class="text-center">
                                    <span class="total_amount">${{ sprintf('%0.2f', $item->item->price * $itemInPack * $item->quantity) }}</span>
                                    <?php $subTotal += $item->item->price * $itemInPack * $item->quantity; ?>
                                </td>

                                <td class="text-center">
                                    <a class="text-danger remove-from-cart btnDelete" href="#"
                                       data-toggle="tooltip"
                                       title="Remove item" data-id="{{ $item->id }}"><img src="{{ asset('themes/cq/images/cross_ic.png') }}" alt=""></a>
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>

            </div>
            <div class="cart_total">
                <div class="row">
                    <div class="col-md-4">

                    </div>
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-6">
                        <ul>
                            <li><span>Total Products:</span><span class="amount sub_total">398.00 USD</span></li>
                            <?php if ( Auth::user()->storeCredit() > 0 ) : ?>
                            <li><span>Store Credits - ${{ number_format(Auth::user()->storeCredit(), 2, '.', '') }}:</span>
                                <input type="text" class="store_credit" placeholder="$" style="width: 75px; text-align: right">
                            </li>
                            <?php endif; ?>
                            <li> <span> Total:  </span><span style="margin-bottom: -1px;" class="total"></span></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
        <div class="cart_gift_recept clearfix">

            <div class="continue_shipping">
                <a href="#" id="btnUpdate">UPDATE CART</a>
                <a href="#" class="btnCheckout">CONTINUE</a>
            </div>
        </div>
    </section>
@stop

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{!! csrf_token() !!}'
                }
            });

            var message = '{{ session('message') }}';

            if (message != '')
                toastr.success(message);

            $('.input_qty').keyup(function () {
                var index = $('.input_qty').index($(this));
                var perPack = parseInt($(this).data('per-pack'));
                var price = $(this).data('price');
                var i = 0;
                var val = $(this).val();

                if (isInt(val)) {
                    i = parseInt(val);

                    if (i < 0)
                        i = 0;
                }


                $('.total_qty:eq('+index+')').html(perPack * i);
                $('.total_amount:eq('+index+')').html('$' + (perPack * i * price).toFixed(2));

                calculate();
            });

            $('#btnUpdate').click(function () {
                var ids = [];
                var qty = [];

                var valid = true;
                $('.input_qty').each(function () {
                    var i = 0;
                    var val = $(this).val();

                    if (isInt(val)) {
                        i = parseInt(val);

                        if (i < 0)
                            return valid = false;
                    } else {
                        return valid = false;
                    }

                    ids.push($(this).data('id'));
                    qty.push(i);
                });

                if (!valid) {
                    alert('Invalid Quantity.');
                    return;
                }

                $.ajax({
                    method: "POST",
                    url: "{{ route('update_cart') }}",
                    data: { ids: ids, qty: qty }
                }).done(function( data ) {
                    window.location.replace("{{ route('update_cart_success') }}");
                });
            });

            $('.btnDelete').click(function () {
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('delete_cart') }}",
                    data: { id: id }
                }).done(function( data ) {
                    location.reload();
                });
            });

            $('.btnCheckout').click(function (e) {
                e.preventDefault();
                var vendorId = [$(this).data('vendor-id')];
                var storeCredit = $('.store_credit').val(); 
                $.ajax({
                    method: "POST",
                    url: "{{ route('create_checkout') }}",
                    data: { storeCredit: storeCredit },
                }).done(function( data ) {
                    if (data.success)
                        window.location.replace("{{ route('show_checkout') }}" + "?id=" + data.message);
                    else
                        alert(data.message);
                });
            });

            function calculate() {
                var subTotal = 0;

                $('.input_qty').each(function () {
                    var perPack = parseInt($(this).data('per-pack'));
                    var price = $(this).data('price');
                    var i = 0;
                    var val = $(this).val();

                    if (isInt(val)) {
                        i = parseInt(val);

                        if (i < 0)
                            i = 0;
                    }

                    subTotal += perPack * i * price;
                });

                var store_credit = parseFloat($('.store_credit').val());

                if(isNaN(store_credit))
                    store_credit = 0;


                var total = subTotal-store_credit;

                if (total < 0)
                    total = 0;


                $('.sub_total').html('$' + subTotal.toFixed(2));
                $('.total').html('$' + total.toFixed(2));
            }

            calculate();

            function isInt(value) {
                return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
            }

            $('.store_credit').keyup(function () {
                calculate();
            });
        });
    </script>
@stop