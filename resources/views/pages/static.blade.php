@extends('layouts.app')

@section('breadcrumbs')
    @if (request()->route()->getName() == 'about_us')
        {{ Breadcrumbs::render('about_us') }}
    @elseif (request()->route()->getName() == 'contact_us')
        {{ Breadcrumbs::render('contact_us') }}
    @elseif (request()->route()->getName() == 'privacy_policy')
        {{ Breadcrumbs::render('privacy_policy') }}
    @elseif (request()->route()->getName() == 'return_info')
        {{ Breadcrumbs::render('return_info') }}
    @elseif (request()->route()->getName() == 'billing_shipping')
        {{ Breadcrumbs::render('billing_shipping') }}
    @elseif (request()->route()->getName() == 'faq')
        {{ Breadcrumbs::render('faq') }}
    @endif
@stop

@section('content')
    <section class="shipping_cart_area">
        <div class="container content">
            <h3>{{ $title or '' }}</h3>
            <hr class="margin-bottom-2x">

            {!! $content !!}
        </div>
    </section>
@stop