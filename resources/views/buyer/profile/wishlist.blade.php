@extends('buyer.layouts.profile')

@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop

@section('profile_content')
    <div class="row">
        <div class="col-md-12 margin-bottom-1x text-right">
            <a href="" class="btn btn-secondary" id="btnSelectAll">Select All</a>
            <a href="" class="btn btn-primary" id="btnAddToCart">Add To Cart</a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <ul class="il-product-container">
                @foreach($items as $item)
                    <li>
                        <div class="il-item-container">
                            <div class="il-item-top">
                                <div class="left">
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input checkbox_item" type="checkbox" id="checkbox_{{ $item->id }}" data-id="{{ $item->id }}">
                                        <label class="custom-control-label" for="checkbox_{{ $item->id }}"></label>
                                    </div>
                                </div>

                                <div class="right">
                                    <div class="il-item-img">
                                        <a href="{{ route('item_details_page', ['item' => $item->id]) }}" class="text-primary il-item-style-no">
                                            @if (sizeof($item->images) > 0)
                                                <img src="{{ asset($item->images[0]->list_image_path) }}" alt="{{ $item->style_no }}">
                                            @else
                                                <img src="{{ asset('images/no-image.png') }}" alt="{{ $item->style_no }}">
                                            @endif
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="il-item-footer text-center">
                                <a href="{{ route('item_details_page', ['item' => $item->id]) }}" class="text-primary il-item-style-no">{{ $item->style_no }}</a>
                                ${{ number_format($item->price, 2, '.', '') }} <br>
                                <span class="text-muted">Min Qty: {{ $item->min_qty }}</span>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@stop
<div class="modal fade" id="color-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabelLarge"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabelLarge">Add Item</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">
                <form id="form_new_item">

                </form>
            </div>

            <div class="modal-footer">
                <button class="btn btn-primary" id="btnItemAdd">Add</button>
            </div>
        </div>
    </div>
</div>

<template id="template-table">
    <div class="modal-item">
        <h5 class="template-item-name"></h5>
        <table class="table table-bordered item_colors_table">

        </table>
    </div>
</template>

@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('.btnRemove').click(function () {
                var id = $(this).data('id');
                $this = $(this);

                $.ajax({
                    method: "POST",
                    url: "{{ route('remove_from_wishlist') }}",
                    data: { id: id }
                }).done(function( data ) {
                    $this.closest('tr').remove();
                    toastr.success('Removed from Wish List.');
                });
            });

            $('#btnAddToCart').click(function (e) {
                e.preventDefault();

                var ids = [];

                $('.checkbox_item').each(function () {
                    if ($(this).is(':checked')) {
                        ids.push($(this).data('id'));
                    }
                });

                if (ids.length > 0) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('wishlist_item_details') }}",
                        data: {id: ids}
                    }).done(function (products) {
                        $('#form_new_item').html('');

                        $.each(products, function (i, product) {
                            var html = $('#template-table').html();
                            var item = $(html);

                            item.find('.template-item-name').html(product.style_no + ' - Min Qty: ' + product.min_qty);

                            $.each(product.colors, function (ci, color) {
                                if (color.image == '')
                                    item.find('.item_colors_table').append('<tr><td></td><th>' + color.name + '</th><td><input name="colors[]" class="input_color" type="text"><input type="hidden" name="ids[]" value="'+product.id+'"><input type="hidden" name="colorIds[]" value="'+color.id+'"></td></tr>');
                                else
                                    item.find('.item_colors_table').append('<tr><td><img src="'+color.image+'" width="30px"></td><th>' + color.name + '</th><td><input name="colors[]" class="input_color" type="text"><input type="hidden" name="ids[]" value="'+product.id+'"><input type="hidden" name="colorIds[]" value="'+color.id+'"></td></tr>');

                            });


                            $('#form_new_item').append(item);
                        });

                        $('#color-modal').modal('show');

                        /*$('#item_colors_table').html('');

                        $.each(product.colors, function (i, color) {
                            $('#item_colors_table').append('<tr><th>' + color.name + '</th><td><input name="colors[' + color.id + ']" class="input_color" type="text"></td></tr>');
                        });*/
                    });
                }
            });

            $('#btnItemAdd').click(function () {
                var error = false;

                $('.input_color').each(function () {
                    if (error)
                        return;

                    var count = $(this).val();

                    if (count != '' && !isInt(count)) {
                        error = true;
                        return alert('Invalid input.');
                    }
                });

                if (!error) {
                    $.ajax({
                        method: "POST",
                        url: "{{ route('wishlist_add_to_cart') }}",
                        data: $('#form_new_item').serialize()
                    }).done(function( data ) {
                        if (data.success)
                            location.reload();
                        else
                            alert(data.message);
                    });
                }
            });

            $('#btnSelectAll').click(function (e) {
                e.preventDefault();
                $('.checkbox_item').prop('checked', true);
            });

            function isInt(value) {
                return !isNaN(value) && (function(x) { return (x | 0) === x; })(parseFloat(value))
            }
        });
    </script>
@stop