<?php use App\Enumeration\OrderStatus; ?>
@extends('layouts.my_account')

@section('content')
    <h2>Welcome, {{ $user->first_name.' '.$user->last_name }}!</h2>

    <div class="row">
        <div class="col-md-12">
            {!! $buyer_home !!}
        </div>
    </div>

    @if (sizeof($orders) > 0)
        <div class="row">
            <div class="col-md-12 margin-bottom-2x">
                <div class="table-responsive">
                    <table class="table table-hover margin-bottom-none">
                        <thead>
                        <tr>
                            <th>Order #</th>
                            <th>Date Purchased</th>
                            <th colspan="2">Status</th>
                            <th>Total</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td><a class="text-medium navi-link" href="{{ route('show_order_details', ['order' => $order->id]) }}">{{ $order->order_number }}</a></td>
                                <td>{{ date('F d, Y', strtotime($order->created_at)) }}</td>
                                <td>
                                    {{ $order->statusText() }}
                                </td>
                                <td>
                                    @if ($order->status == OrderStatus::$BACK_ORDER && $order->rejected == 0)
                                        <a href="" class="text-success" id="btnApprove" data-id="{{ $order->id }}">Approve</a>
                                        <a href="" class="text-danger" id="btnDecline" data-id="{{ $order->id }}">Decline</a>
                                    @endif
                                </td>
                                <td><span class="text-medium">${{ sprintf('%0.2f', $order->total) }}</span></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    @endif
@stop

@section('additionalJS')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{!! csrf_token() !!}'
                }
            });

            $('#btnApprove').click(function (e) {
                e.preventDefault();
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('order_reject_status_change') }}",
                    data: { id: id, status: 2 },
                }).done(function( data ) {
                    window.location.reload(true);
                });
            });

            $('#btnDecline').click(function (e) {
                e.preventDefault();
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('order_reject_status_change') }}",
                    data: { id: id, status: 1 },
                }).done(function( data ) {
                    window.location.reload(true);
                });
            });
        });
    </script>
@stop