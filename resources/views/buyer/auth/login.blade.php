<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel='stylesheet' href='{{ asset('themes/cq/fonts/stylesheet.css') }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/owl.theme.default.css') }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/owl.carousel.css') }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/slick.css') }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/magnific-popup.css') }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/main.css') }}'/>
    <link rel='stylesheet' href='{{ asset('themes/cq/css/custom.css') }}'/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

</head>
<body class="login_page">
    <!-- Header -->
    @include('layouts.shared.header')
    <!-- Header -->

    <!-- Content -->
    <section class="login_area clearfix">
        <div class="login_wrapper">
            <div class="login_inner ">
                <h2>I am already a registered CQBYCQ user</h2>
                <p>Enter your email address and password to log in.</p>
                <form class="login-box" method="post" action="{{ route('buyer_login_post') }}">
                    @csrf
                    <div class="login_inner_form">
                        <div class="form-group l_f_input">
                            <input type="email" name="email" id="email" class="form-control" required>
                            <label class="form-control-placeholder" for="name">Email</label>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" id="password" class="form-control" required>
                            <label class="form-control-placeholder" for="password">Password</label>
                        </div>

                        <div class="custom_checkbox">
                            <input type="checkbox" id="remember_me" name="remember_me" checked>
                            <label for="remember_me" style="font-size: 14px">Remember me</label>
                        </div>

                        <p class="tex alert-info">{{ session('message') }}</p>
                        <a href="{{ route('password_reset_buyer') }}">Forgot your password?</a>
                        <p>
                            <button class="cq_btn medium">LOG IN</button>
                        </p>
                    </div>
                </form>
            </div>
            <div class="login_inner ">
                <div class="login_inner_form">
                    <h2>I want a CQBYCQ user account</h2>
                    <p>If you still don't have a <b>CQBYCQ.com</b> account, use this option to access the registration form.</p>
                    <p>Provide your details to make <b>CQBYCQ.com</b> purchases easier.</p>
                    <a href="{{ route('buyer_register') }}" class="btn btn-primary">CREATE ACCOUNT</a>
                </div>
            </div>
        </div>
    </section>
    <!-- Content -->

    <!-- Footer -->
    @include('layouts.shared.footer')
    <!-- Footer -->
</body>
</html>
